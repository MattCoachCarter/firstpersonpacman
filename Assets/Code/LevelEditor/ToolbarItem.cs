using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class ToolbarItem : LevelEditorUIGuard
{
    public string prefab_name = "";
    public int level_data_const = LevelData.EMPTY;
    public bool special = false;
    public string special_key = "";
    public int hotkey = -1;
    private Color highlight_color = new Color(1f, 0f, 0f, 1f);
    private Color unhighlight_color = new Color(1f, 1f, 1f, 1f);


    // Start is called before the first frame update
    void Start()
    {
        /*
        GameObject preview_obj = Common.Spawn(prefab_name, new Vector3(-1000, -1000, -1000));
        
        if(preview_obj.GetComponent<Pellet>())
        {
            Destroy(preview_obj.GetComponent<Pellet>());
        }

        Texture2D preview_img = AssetPreview.GetAssetPreview(preview_obj);
        SetChildRawImage("Image", preview_img);
        Common.SafeDestroy(preview_obj);
        */
    }


    // Update is called once per frame
    void Update()
    {
        CheckForHotkey();
    }


    public void AssignAsCurrent()
    {
        LevelEditor.SetCurrentItem(this);
        Highlight();
    }


    public void Highlight()
    {
        GetComponent<Button>().Select();
        SetChildTextColor("Hotkey", highlight_color);
        SetChildTextColor("Name", highlight_color);
    }


    public void Unhighlight()
    {
        // GetComponent<Button>().Select();
        SetChildTextColor("Hotkey", unhighlight_color);
        SetChildTextColor("Name", unhighlight_color);
    }


    private void CheckForHotkey()
    {
        bool should_assign = false;

        should_assign = should_assign || (hotkey == 0 && InputManager.WasJustPressed(BTNINPUT.hotkey0));
        should_assign = should_assign || (hotkey == 1 && InputManager.WasJustPressed(BTNINPUT.hotkey1));
        should_assign = should_assign || (hotkey == 2 && InputManager.WasJustPressed(BTNINPUT.hotkey2));
        should_assign = should_assign || (hotkey == 3 && InputManager.WasJustPressed(BTNINPUT.hotkey3));
        should_assign = should_assign || (hotkey == 4 && InputManager.WasJustPressed(BTNINPUT.hotkey4));
        should_assign = should_assign || (hotkey == 5 && InputManager.WasJustPressed(BTNINPUT.hotkey5));
        should_assign = should_assign || (hotkey == 6 && InputManager.WasJustPressed(BTNINPUT.hotkey6));
        should_assign = should_assign || (hotkey == 7 && InputManager.WasJustPressed(BTNINPUT.hotkey7));
        should_assign = should_assign || (hotkey == 8 && InputManager.WasJustPressed(BTNINPUT.hotkey8));
        should_assign = should_assign || (hotkey == 9 && InputManager.WasJustPressed(BTNINPUT.hotkey9));

        if(should_assign)
        {
            AssignAsCurrent();
        }
    }
}
