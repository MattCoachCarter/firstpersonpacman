using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitConfirmer : LevelEditorUIGuard
{
    public static ExitConfirmer instance;


    void Start()
    {
        instance = this;
        gameObject.SetActive(false);
    }


    void Update()
    {
        if(InputManager.WasJustPressed(BTNINPUT.pause))
        {
            CancelClicked();
        }
    }


    public void ExitClicked()
    {
        LevelEditor.ExitConfirmed();
    }


    public void CancelClicked()
    {
        gameObject.SetActive(false);
        LevelEditor.ResetPlacementEnablement();
    }
}
