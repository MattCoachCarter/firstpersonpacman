using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceholderDelegate : MonoBehaviour
{
    private Placeholder parent = null;


    public void Setup(Placeholder p)
    {
        parent = p;
    }


    void OnMouseEnter()
    {
        parent.PropagateHover();
    }

    
    void OnMouseExit()
    {
        parent.PropagateUnhover();
    }
}
