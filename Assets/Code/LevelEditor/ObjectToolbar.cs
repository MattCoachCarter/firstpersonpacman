using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectToolbar : EditorToolbar
{
    public new static ObjectToolbar instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public static void Activate()
    {
        instance.DoActivate();
    }


    protected void DoActivate()
    {
        gameObject.SetActive(true);
    }


    public static void Deactivate()
    {
        instance.DoDeactivate();
    }


    protected void DoDeactivate()
    {
        gameObject.SetActive(false);
    }
}
