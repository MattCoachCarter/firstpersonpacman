using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelValidationErrorDisplay : LevelEditorUIGuard
{
    public static LevelValidationErrorDisplay instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OKClicked()
    {
        Deactivate();
    }


    private void DisplayErrors(List<string> errors)
    {
        string err_text = "";
        foreach(string error in errors)
        {
            if(err_text != "")
            {
                err_text += "\n";
            }
            err_text += "* "+ error;
        }

        GetChild("ValidationErrorsContainer").GetComponent<UIHelper>().SetChildText("ValidationErrors", err_text);
    }


    public static void Deactivate()
    {
        instance.gameObject.SetActive(false);
        LevelEditor.ResetPlacementEnablement();
    }


    public static void Activate(List<string> errors)
    {
        instance.gameObject.SetActive(true);
        instance.DisplayErrors(errors);
    }
}
