using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSplashScreen : LevelEditorUIGuard
{
    public static SaveSplashScreen instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public static void Deactivate()
    {
        instance.gameObject.SetActive(false);
        LevelEditor.ResetPlacementEnablement();
    }


    public static void Activate()
    {
        instance.gameObject.SetActive(true);
    }
}
