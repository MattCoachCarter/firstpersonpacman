using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class LevelEditorUIGuard : UIHelper
     , IPointerClickHandler
     , IDragHandler
     , IPointerEnterHandler
     , IPointerExitHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {}
 
    public void OnDrag(PointerEventData eventData)
    {}

    public void OnPointerEnter(PointerEventData eventData)
    {
        LevelEditor.DisablePlacement();
    }
 

    public void OnPointerExit(PointerEventData eventData)
    {
        LevelEditor.EnablePlacement();
    }
}
