using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewFileSaveConfirm : LevelEditorUIGuard
{
    public static NewFileSaveConfirm instance;
    private InputField difficulty_input;
    private InputField name_input;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        difficulty_input = GetChild("DifficultyInput").GetComponent<InputField>();
        name_input = GetChild("NameInput").GetComponent<InputField>();
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        CheckForSaveEnablement();
    }


    public static void Deactivate()
    {
        instance.gameObject.SetActive(false);
        LevelEditor.ResetPlacementEnablement();
    }


    public static void Activate()
    {
        instance.SetNameVal("");
        instance.SetDifficultyVal("1");
        instance.gameObject.SetActive(true);
    }


    public void SaveClicked()
    {
        if(IsInputValid())
        {
            LevelEditor.NewSaveConfirmed(GetNameVal(), GetDifficultyValInt());
        }
    }


    public void CancelClicked()
    {
        Deactivate();
    }


    private void SetNameVal(string val)
    {
        name_input.text = val;
    }


    private void SetDifficultyVal(string val)
    {
        difficulty_input.text = val;
    }


    private string GetNameVal()
    {
        return name_input.text.Replace(".", "").Replace("/", "").Replace('\\', '\0').Replace(" ", "_").ToLower();
    }


    private string GetDifficultyVal()
    {
        return difficulty_input.text;
    }


    private int GetDifficultyValInt()
    {
        int return_value = -1;
        bool parse_success = int.TryParse(GetDifficultyVal(), out return_value);
        if(!parse_success)
        {
            return_value = -1;
        }

        return return_value;
    }


    private bool IsInputValid()
    {
        return (GetNameVal().Length > 0 &&
                GetDifficultyVal().Length > 0 &&
                GetDifficultyValInt() >= 0);
    }


    private void CheckForSaveEnablement()
    {
        if(IsInputValid())
        {
            ActivateChild("SaveButton");
        }
        else
        {
            DeactivateChild("SaveButton");
        }
    }
}
