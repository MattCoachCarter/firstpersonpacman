using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetToolbar : EditorToolbar
{
    public new static TargetToolbar instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public static void Activate()
    {
        instance.DoActivate();
    }


    protected void DoActivate()
    {
        gameObject.SetActive(true);
    }


    public static void Deactivate()
    {
        instance.DoDeactivate();
    }


    protected void DoDeactivate()
    {
        gameObject.SetActive(false);
    }
}
