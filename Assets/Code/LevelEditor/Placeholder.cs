using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placeholder : MonoBehaviour
{
    private bool ready = false;
    private int level_x;
    private int level_y;
    private LevelEditor parent;
    private bool hovering = false;
    private bool propagate_hovering = false;
    private GameObject spawned_object = null;
    private GameObject spawned_target = null;
    private GameObject spawned_spawn = null;
    private bool clicked_this_hover = false;

    private Material default_material;
    private Material hover_material;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckForClick();
        if(!AnyHovering())
        {
            clicked_this_hover = false;
            HoverStyling();
        }
        else if(HotKeyPressed())
        {
            clicked_this_hover = false;
        }
    }


    private bool HotKeyPressed()
    {
        bool pressed = false;
        pressed = pressed || (InputManager.WasJustPressed(BTNINPUT.hotkey0));
        pressed = pressed || (InputManager.WasJustPressed(BTNINPUT.hotkey1));
        pressed = pressed || (InputManager.WasJustPressed(BTNINPUT.hotkey2));
        pressed = pressed || (InputManager.WasJustPressed(BTNINPUT.hotkey3));
        pressed = pressed || (InputManager.WasJustPressed(BTNINPUT.hotkey4));
        pressed = pressed || (InputManager.WasJustPressed(BTNINPUT.hotkey5));
        pressed = pressed || (InputManager.WasJustPressed(BTNINPUT.hotkey6));
        pressed = pressed || (InputManager.WasJustPressed(BTNINPUT.hotkey7));
        pressed = pressed || (InputManager.WasJustPressed(BTNINPUT.hotkey8));
        pressed = pressed || (InputManager.WasJustPressed(BTNINPUT.hotkey9));

        return pressed;
    }


    public void PropagateHover()
    {
        propagate_hovering = true;
        HoverStyling();
    }


    public void PropagateUnhover()
    {
        propagate_hovering = false;
        HoverStyling();
    }


    public void SetSpawnedObject(GameObject obj)
    {
        DestroySpawnedObject();
        spawned_object = obj;
        spawned_object.AddComponent<PlaceholderDelegate>();
        spawned_object.GetComponent<PlaceholderDelegate>().Setup(this);
    }


    public void SetSpawnedTarget(GameObject obj)
    {
        DestroySpawnedTarget();
        spawned_target = obj;
        spawned_target.AddComponent<PlaceholderDelegate>();
        spawned_target.GetComponent<PlaceholderDelegate>().Setup(this);
    }


    public void SetSpawnedSpawn(GameObject obj)
    {
        DestroySpawnedSpawn();
        spawned_spawn = obj;
        spawned_spawn.AddComponent<PlaceholderDelegate>();
        spawned_spawn.GetComponent<PlaceholderDelegate>().Setup(this);
    }


    public void DestroySpawnedObject()
    {
        Common.SafeDestroy(spawned_object);
        propagate_hovering = false;
    }


    public void DestroySpawnedTarget()
    {
        Common.SafeDestroy(spawned_target);
        propagate_hovering = false;
    }


    public void DestroySpawnedSpawn()
    {
        Common.SafeDestroy(spawned_spawn);
        propagate_hovering = false;
    }


    public GameObject GetSpawnedObject()
    {
        return spawned_object;
    }


    public GameObject GetSpawnedTarget()
    {
        return spawned_target;
    }


    public GameObject GetSpawnedSpawn()
    {
        return spawned_spawn;
    }


    private string GetDefaultMaterialName()
    {
        if(level_x % 5 == 0 || level_y % 5 == 0)
        {
            return "Materials/placeholder_alt";
        }
        return "Materials/placeholder";
    }


    private void LoadMaterials()
    {
        default_material = Common.LoadMaterial(GetDefaultMaterialName());
        hover_material = Common.LoadMaterial("Materials/placeholder_hover");
        SetMaterial(default_material);
    }


    private void CheckForClick()
    {
        if(!clicked_this_hover && Input.GetMouseButton(0) && AnyHovering())
        {
            clicked_this_hover = true;
            LevelEditor.Place(this);
        }
        else if(!clicked_this_hover && Input.GetMouseButton(1) && AnyHovering())
        {
            clicked_this_hover = true;
            LevelEditor.Unplace(this);
        }
    }


    public void Setup(int x, int y, LevelEditor p)
    {
        level_x = x;
        level_y = y;
        parent = p;
        LoadMaterials();
        ready = true;
    }


    public int GetX()
    {
        return level_x;
    }


    public int GetY()
    {
        return level_y;
    }


    void OnMouseEnter()
    {
        if(ready)
        {
            hovering = true;
            HoverStyling();
        }
    }

    
    void OnMouseExit()
    {
        if(ready)
        {
            hovering = false;
            HoverStyling();
        }
    }


    private bool AnyHovering()
    {
        return hovering || propagate_hovering;
    }


    private void HoverStyling()
    {
        if(AnyHovering())
        {
            SetMaterial(hover_material);
        }
        else
        {
            SetMaterial(default_material);
        }
    }


    private void SetMaterial(Material m)
    {
        GameObject model = transform.Find("Cube").gameObject;
        model.GetComponent<Renderer>().material = m;
    }
}
