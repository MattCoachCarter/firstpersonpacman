using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveConfirm : LevelEditorUIGuard
{
    public static SaveConfirm instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SaveClicked()
    {
        LevelEditor.SaveConfirmed();
    }


    public void CancelClicked()
    {
        Deactivate();
    }


    public static void Deactivate()
    {
        instance.gameObject.SetActive(false);
        LevelEditor.ResetPlacementEnablement();
    }


    public static void Activate(string level_name)
    {
        instance.gameObject.SetActive(true);
        instance.SetChildText("Prompt", "Are you sure you wish to overwrite the level: "+ level_name);
    }
}
