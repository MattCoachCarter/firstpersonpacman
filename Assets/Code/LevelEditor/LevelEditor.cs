using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditor : LevelBase
{
    private string level_name = null;
    private string level_file = null;
    private const float CAMERA_SPEED_SLOW = 50f;
    private const float CAMERA_SPEED_FAST = 200f;
    private GameObject camera_handle;
    public static LevelEditor instance;
    private GameObject floor = null;
    private Vector3 camera_start_position = new Vector3(75, 100, 35);
    private Vector3 camera_start_angles = new Vector3(90, 0, 0);
    private string current_place_mode = "object";
    private ToolbarItem current_item = null;
    private int placement_enables = 0;
    private int placement_disables = 0;
    private List<GameObject> object_objects;
    private List<GameObject> spawn_objects;
    private List<GameObject> target_objects;
    private Placeholder player_spawn_placeholder = null;
    private Placeholder blinky_spawn_placeholder = null;
    private Placeholder pinky_spawn_placeholder = null;
    private Placeholder inky_spawn_placeholder = null;
    private Placeholder clyde_spawn_placeholder = null;
    private Placeholder blinky_scatter_placeholder = null;
    private Placeholder pinky_scatter_placeholder = null;
    private Placeholder inky_scatter_placeholder = null;
    private Placeholder clyde_scatter_placeholder = null;
    private Placeholder ghost_exit_placeholder = null;
    private Placeholder eaten_recover_placeholder = null;
    private List<string> level_validation_errors = null;
    private bool new_level_data = false;
    private bool x_symmetry = false;
    private bool y_symmetry = false;

 
    // Start is called before the first frame update
    void Start()
    {
        if(instance != null)
        {
            Debug.LogWarning("LevelEditor instance was not null");
        }
        instance = this;
        FindCamera();
        Setup();
    }


    // Update is called once per frame
    void Update()
    {
        LoadingSplashScreen.Hide();
        HandleInput();
    }


    public void TestClicked()
    {
        if(ValidateLevelData())
        {
            JSONParser.WriteTestLevelData(level_data);
            if(level_name != null)
            {
                JSONParser.WriteTestLevelName(level_name);
            }
            GameManager.TestLevel(level_data);

        }
        else
        {
            LevelValidationErrorDisplay.Activate(level_validation_errors);
        }
    }


    public void SaveClicked()
    {
        if(ValidateLevelData())
        {
            if(level_name != "" && level_name != null)
            {
                SaveConfirm.Activate(level_name);
            }
            else
            {
                NewFileSaveConfirm.Activate();
            }
        }
        else
        {
            LevelValidationErrorDisplay.Activate(level_validation_errors);
        }
    }


    public static void ResetPlacementEnablement()
    {
        if(instance != null)
        {
            instance.placement_enables = 0;
            instance.placement_disables = 0;
        }
    }


    private bool ValidateLevelData()
    {
        level_validation_errors = new List<string>();

        int pellet_count = 0;
        int portals_seen = 0;
        foreach(int level_data_const in level_data.layout)
        {
            if(level_data_const == LevelData.PELLET || level_data_const == LevelData.POWER_PELLET)
            {
                pellet_count++;
            }
            else if(level_data_const == LevelData.PORTAL)
            {
                portals_seen++;
            }
        }

        if(pellet_count <= 0)
        {
            level_validation_errors.Add("Level requires at least one pellet");
        }
        if(portals_seen == 1)
        {
            level_validation_errors.Add("Single portal is invalid, require exactly 0 or 2");
        }

        if(level_data.blinky_start_x == -1 || level_data.blinky_start_y == -1)
        {
            level_validation_errors.Add("Blinky spawn not set");
        }
        else if(level_data.IsSolidOrPortal(level_data.blinky_start_x, level_data.blinky_start_y))
        {
            level_validation_errors.Add("Blinky spawn set to space with a solid object or portal");
        }

        if(level_data.pinky_start_x == -1 || level_data.pinky_start_y == -1)
        {
            level_validation_errors.Add("Pinky spawn not set");
        }
        else if(level_data.IsSolidOrPortal(level_data.pinky_start_x, level_data.pinky_start_y))
        {
            level_validation_errors.Add("Pinky spawn set to space with a solid object or portal");
        }

        if(level_data.inky_start_x == -1 || level_data.inky_start_y == -1)
        {
            level_validation_errors.Add("Inky spawn not set");
        }
        else if(level_data.IsSolidOrPortal(level_data.inky_start_x, level_data.inky_start_y))
        {
            level_validation_errors.Add("Inky spawn set to space with a solid object or portal");
        }

        if(level_data.clyde_start_x == -1 || level_data.clyde_start_y == -1)
        {
            level_validation_errors.Add("Clyde spawn not set");
        }
        else if(level_data.IsSolidOrPortal(level_data.clyde_start_x, level_data.clyde_start_y))
        {
            level_validation_errors.Add("Clyde spawn set to space with a solid object or portal");
        }

        if(level_data.ghost_house_exit_x == -1 || level_data.ghost_house_exit_y == -1)
        {
            level_validation_errors.Add("Ghost exit not set");
        }
        else if(level_data.IsSolidOrPortal(level_data.ghost_house_exit_x, level_data.ghost_house_exit_y))
        {
            level_validation_errors.Add("Ghost exit set to space with a solid object or portal");
        }

        if(level_data.eaten_recovery_x == -1 || level_data.eaten_recovery_y == -1)
        {
            level_validation_errors.Add("Ghost recovery point not set");
        }
        else if(level_data.IsSolidOrPortal(level_data.eaten_recovery_x, level_data.eaten_recovery_y))
        {
            level_validation_errors.Add("Ghost recovery point set to space with a solid object or portal");
        }

        if(level_data.spawn_x == -1 || level_data.spawn_y == -1)
        {
            level_validation_errors.Add("Player spawn point not set");
        }
        else if(level_data.IsSolidOrPortal(level_data.spawn_x, level_data.spawn_y))
        {
            level_validation_errors.Add("Player spawn point set to space with a solid object or portal");
        }

        if(level_data.blinky_scatter_x == -1 || level_data.blinky_scatter_y == -1)
        {
            level_validation_errors.Add("Blinky scatter target not set");
        }

        if(level_data.pinky_scatter_x == -1 || level_data.pinky_scatter_y == -1)
        {
            level_validation_errors.Add("Pinky scatter target not set");
        }

        if(level_data.inky_scatter_x == -1 || level_data.inky_scatter_y == -1)
        {
            level_validation_errors.Add("Inky scatter target not set");
        }

        if(level_data.clyde_scatter_x == -1 || level_data.clyde_scatter_y == -1)
        {
            level_validation_errors.Add("Clyde scatter target not set");
        }

        return (level_validation_errors.Count == 0);
    }


    public static void NewSaveConfirmed(string name, int difficulty)
    {
        NewFileSaveConfirm.Deactivate();
        SaveSplashScreen.Activate();
        instance.level_data.difficulty = difficulty;
        instance.level_name = name;
        JSONParser.WriteLevelData(name, instance.level_data);
        SaveSplashScreen.Deactivate();
        SavedNotification.Activate();
    }


    public static void SaveConfirmed()
    {
        SaveConfirm.Deactivate();
        SaveSplashScreen.Activate();
        JSONParser.WriteLevelData(instance.level_name, instance.level_data);
        SaveSplashScreen.Deactivate();
        SavedNotification.Activate();
    }


    public void ExitClicked()
    {
        Debug.Log("ExitClicked");
        ExitConfirmer.instance.gameObject.SetActive(true);
    }


    public static void ExitConfirmed()
    {
        GameManager.ReturnToMainMenu();
    }


    public static void EnablePlacement()
    {
        instance.placement_enables++;
    }


    public static void DisablePlacement()
    {
        instance.placement_disables++;
    }


    private bool PlacementEnabled()
    {
        return (placement_enables - placement_disables) >= 0;
    }


    public static void Place(Placeholder placeholder)
    {
        if(instance.current_item != null && instance.PlacementEnabled())
        {
            instance.DoPlace(placeholder);
        }
    }


    public static void Unplace(Placeholder placeholder)
    {
        if(instance.PlacementEnabled())
        {
            instance.DoUnplace(placeholder);
        }
    }


    private void DoPlace(Placeholder placeholder)
    {
        if(current_place_mode == "object")
        {
            PlaceObject(placeholder);
        }
        else if(current_place_mode == "target")
        {
            PlaceTarget(placeholder);
        }
        else // spawn
        {
            PlaceSpawn(placeholder);
        }
    }


    private void DoUnplace(Placeholder placeholder)
    {
        if(current_place_mode == "object")
        {
            UnplaceObject(placeholder);
        }
        else if(current_place_mode == "target")
        {
            UnplaceTarget(placeholder);
        }
        else // spawn
        {
            UnplaceSpawn(placeholder);
        }
    }


    private GameObject SafeSpawnObject(string prefab_name, Vector3 spawn_pos)
    {
        GameObject spawned_game_obj = Common.Spawn(prefab_name, spawn_pos);

        if(spawned_game_obj.GetComponent<Pellet>() != null)
        {
            Destroy(spawned_game_obj.GetComponent<Pellet>());
        }
        else if(spawned_game_obj.GetComponent<Player>() != null)
        {
            Destroy(spawned_game_obj.GetComponent<Player>());
        }
        else if(spawned_game_obj.GetComponent<Ghost>() != null)
        {
            Destroy(spawned_game_obj.GetComponent<Ghost>());
        }

        return spawned_game_obj;
    }


    private Placeholder GetPlaceholderFromLevelCoords(int x, int y)
    {
        foreach(GameObject placeholder_game_obj in GameObject.FindGameObjectsWithTag("placeholder"))
        {
            Placeholder p = placeholder_game_obj.GetComponent<Placeholder>();
            if(p.GetX() == x && p.GetY() == y)
            {
                return p;
            }
        }
        return null;
    }


    private void PlaceObject(Placeholder placeholder)
    {
        PlaceObject(placeholder, current_item.level_data_const, current_item.prefab_name);
    }


    private void PlaceObject(Placeholder placeholder, int level_data_constant, string prefab_name)
    {
        PlaceObject(placeholder, level_data_constant, prefab_name, false);
    }


    private void PlaceObject(Placeholder placeholder, int level_data_constant, string prefab_name, bool ignore_symmerty)
    {
        Placeholder other = null;
        if(x_symmetry && !ignore_symmerty)
        {
            Debug.Log("x_symmetry");
            int other_x = (level_data.level_width - 1) - placeholder.GetX();
            int other_y = placeholder.GetY();

            if(other_x != placeholder.GetX())
            {
                other = GetPlaceholderFromLevelCoords(other_x, other_y);
                PlaceObject(other, level_data_constant, prefab_name, true);
            }
        }

        if(y_symmetry && !ignore_symmerty)
        {
            Debug.Log("y_symmetry");
            int other_x = placeholder.GetX();
            int other_y = (level_data.level_height - 1) - placeholder.GetY();

            if(other_y != placeholder.GetY())
            {
                other = GetPlaceholderFromLevelCoords(other_x, other_y);
                PlaceObject(other, level_data_constant, prefab_name, true);
            }
        }

        if(x_symmetry && y_symmetry && !ignore_symmerty)
        {
            Debug.Log("xy_symmetry");
            int other_x = (level_data.level_width - 1) - placeholder.GetX();
            int other_y = (level_data.level_height - 1) - placeholder.GetY();

            if(other_x != placeholder.GetX() && other_y != placeholder.GetY())
            {
                other = GetPlaceholderFromLevelCoords(other_x, other_y);
                PlaceObject(other, level_data_constant, prefab_name, true);
            }
        }

        Vector3 spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(placeholder.GetX(),
                                                                   placeholder.GetY(),
                                                                   GetSpawnY(level_data_constant));
        GameObject spawned_game_obj = SafeSpawnObject(prefab_name, spawn_pos);
        placeholder.SetSpawnedObject(spawned_game_obj);
        level_data.SetData(placeholder.GetX(), placeholder.GetY(), level_data_constant);
        object_objects.Add(spawned_game_obj);
    }


    private void PlaceTarget(Placeholder placeholder)
    {
        PlaceTarget(placeholder, current_item.special_key, current_item.prefab_name);
    }


    private void PlaceTarget(Placeholder placeholder, string special_key, string prefab_name)
    {
        UnplaceTarget(placeholder);
        Vector3 spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(placeholder.GetX(),
                                                                   placeholder.GetY(),
                                                                   1f);
        spawn_pos.x += ((float)GameConfig.SPACE_SIZE) / 5f;
        spawn_pos.z += ((float)GameConfig.SPACE_SIZE) / 5f;
        GameObject spawned_game_obj = Common.Spawn(prefab_name, spawn_pos);
        placeholder.SetSpawnedTarget(spawned_game_obj);

        if(special_key == "blinky_scatter")
        {
            if(blinky_scatter_placeholder != null)
            {
                blinky_scatter_placeholder.DestroySpawnedTarget();
            }
            level_data.blinky_scatter_x = placeholder.GetX();
            level_data.blinky_scatter_y = placeholder.GetY();
            blinky_scatter_placeholder = placeholder;
        }
        else if(special_key == "pinky_scatter")
        {
            if(pinky_scatter_placeholder != null)
            {
                pinky_scatter_placeholder.DestroySpawnedTarget();
            }
            level_data.pinky_scatter_x = placeholder.GetX();
            level_data.pinky_scatter_y = placeholder.GetY();
            pinky_scatter_placeholder = placeholder;
        }
        else if(special_key == "inky_scatter")
        {
            if(inky_scatter_placeholder != null)
            {
                inky_scatter_placeholder.DestroySpawnedTarget();
            }
            level_data.inky_scatter_x = placeholder.GetX();
            level_data.inky_scatter_y = placeholder.GetY();
            inky_scatter_placeholder = placeholder;
        }
        else if(special_key == "clyde_scatter")
        {
            if(clyde_scatter_placeholder != null)
            {
                clyde_scatter_placeholder.DestroySpawnedTarget();
            }
            level_data.clyde_scatter_x = placeholder.GetX();
            level_data.clyde_scatter_y = placeholder.GetY();
            clyde_scatter_placeholder = placeholder;
        }
        else if(special_key == "ghost_exit")
        {
            if(ghost_exit_placeholder != null)
            {
                ghost_exit_placeholder.DestroySpawnedTarget();
            }
            level_data.ghost_house_exit_x = placeholder.GetX();
            level_data.ghost_house_exit_y = placeholder.GetY();
            ghost_exit_placeholder = placeholder;
        }
        else if(special_key == "eaten_recovery")
        {
            if(eaten_recover_placeholder != null)
            {
                eaten_recover_placeholder.DestroySpawnedTarget();
            }
            level_data.eaten_recovery_x = placeholder.GetX();
            level_data.eaten_recovery_y = placeholder.GetY();
            eaten_recover_placeholder = placeholder;
        }
        else
        {
            Debug.LogError("Unknown target key: "+ special_key);
        }

        target_objects.Add(spawned_game_obj);
    }


    private void PlaceSpawn(Placeholder placeholder)
    {
        PlaceSpawn(placeholder, current_item.special_key, current_item.prefab_name);
    }


    private void PlaceSpawn(Placeholder placeholder, string special_key, string prefab_name)
    {
        UnplaceSpawn(placeholder);
        Vector3 spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(placeholder.GetX(),
                                                                   placeholder.GetY(),
                                                                   1f);
        spawn_pos.x -= ((float)GameConfig.SPACE_SIZE) / 5f;
        spawn_pos.z -= ((float)GameConfig.SPACE_SIZE) / 5f;
        GameObject spawned_game_obj = Common.Spawn(prefab_name, spawn_pos);
        placeholder.SetSpawnedSpawn(spawned_game_obj);

        if(special_key == "player_spawn")
        {
            if(player_spawn_placeholder != null)
            {
                player_spawn_placeholder.DestroySpawnedSpawn();
            }
            level_data.spawn_x = placeholder.GetX();
            level_data.spawn_y = placeholder.GetY();
            player_spawn_placeholder = placeholder;
        }
        else if(special_key == "blinky_spawn")
        {
            if(blinky_spawn_placeholder != null)
            {
                blinky_spawn_placeholder.DestroySpawnedSpawn();
            }
            level_data.blinky_start_x = placeholder.GetX();
            level_data.blinky_start_y = placeholder.GetY();
            blinky_spawn_placeholder = placeholder;
        }
        else if(special_key == "pinky_spawn")
        {
            if(pinky_spawn_placeholder != null)
            {
                pinky_spawn_placeholder.DestroySpawnedSpawn();
            }
            level_data.pinky_start_x = placeholder.GetX();
            level_data.pinky_start_y = placeholder.GetY();
            pinky_spawn_placeholder = placeholder;
        }
        else if(special_key == "inky_spawn")
        {
            if(inky_spawn_placeholder != null)
            {
                inky_spawn_placeholder.DestroySpawnedSpawn();
            }
            level_data.inky_start_x = placeholder.GetX();
            level_data.inky_start_y = placeholder.GetY();
            inky_spawn_placeholder = placeholder;
        }
        else if(special_key == "clyde_spawn")
        {
            if(clyde_spawn_placeholder != null)
            {
                clyde_spawn_placeholder.DestroySpawnedSpawn();
            }
            level_data.clyde_start_x = placeholder.GetX();
            level_data.clyde_start_y = placeholder.GetY();
            clyde_spawn_placeholder = placeholder;
        }
        else
        {
            Debug.LogError("Unknown spawn key: "+ special_key);
        }

        spawn_objects.Add(spawned_game_obj);
    }


    private void UnplaceObject(Placeholder placeholder)
    {
        placeholder.DestroySpawnedObject();
        level_data.SetData(placeholder.GetX(), placeholder.GetY(), LevelData.EMPTY);
    }


    private void UnplaceTarget(Placeholder placeholder)
    {
        if(blinky_scatter_placeholder == placeholder)
        {
            placeholder.DestroySpawnedTarget();
            level_data.blinky_scatter_x= -1;
            level_data.blinky_scatter_y= -1;
            blinky_scatter_placeholder = null;
        }
        else if(pinky_scatter_placeholder == placeholder)
        {
            placeholder.DestroySpawnedTarget();
            level_data.pinky_scatter_x= -1;
            level_data.pinky_scatter_y= -1;
            pinky_scatter_placeholder = null;
        }
        else if(inky_scatter_placeholder == placeholder)
        {
            placeholder.DestroySpawnedTarget();
            level_data.inky_scatter_x= -1;
            level_data.inky_scatter_y= -1;
            inky_scatter_placeholder = null;
        }
        else if(clyde_scatter_placeholder == placeholder)
        {
            placeholder.DestroySpawnedTarget();
            level_data.clyde_scatter_x= -1;
            level_data.clyde_scatter_y= -1;
            clyde_scatter_placeholder = null;
        }
        else if(ghost_exit_placeholder == placeholder)
        {
            placeholder.DestroySpawnedTarget();
            level_data.ghost_house_exit_x= -1;
            level_data.ghost_house_exit_y= -1;
            ghost_exit_placeholder = null;
        }
        else if(eaten_recover_placeholder == placeholder)
        {
            placeholder.DestroySpawnedTarget();
            level_data.eaten_recovery_x= -1;
            level_data.eaten_recovery_y= -1;
            eaten_recover_placeholder = null;
        }
    }


    private void UnplaceSpawn(Placeholder placeholder)
    {
        if(player_spawn_placeholder == placeholder)
        {
            placeholder.DestroySpawnedSpawn();
            level_data.spawn_x= -1;
            level_data.spawn_y= -1;
            player_spawn_placeholder = null;
        }
        else if(blinky_spawn_placeholder == placeholder)
        {
            placeholder.DestroySpawnedSpawn();
            level_data.blinky_start_x= -1;
            level_data.blinky_start_y= -1;
            blinky_spawn_placeholder = null;
        }
        else if(pinky_spawn_placeholder == placeholder)
        {
            placeholder.DestroySpawnedSpawn();
            level_data.pinky_start_x= -1;
            level_data.pinky_start_y= -1;
            pinky_spawn_placeholder = null;
        }
        else if(inky_spawn_placeholder == placeholder)
        {
            placeholder.DestroySpawnedSpawn();
            level_data.inky_start_x= -1;
            level_data.inky_start_y= -1;
            inky_spawn_placeholder = null;
        }
        else if(clyde_spawn_placeholder == placeholder)
        {
            placeholder.DestroySpawnedSpawn();
            level_data.clyde_start_x= -1;
            level_data.clyde_start_y= -1;
            clyde_spawn_placeholder = null;
        }
    }


    public static void SetCurrentItem(ToolbarItem new_item)
    {
        instance.UnsetCurrentItem();
        instance.current_item = new_item;
    }


    private void UnsetCurrentItem()
    {
        if(current_item != null)
        {
            current_item.Unhighlight();
        }
        current_item = null;
    }


    public void SwitchMode(string new_mode)
    {
        if(new_mode != current_place_mode)
        {
            current_place_mode = new_mode;
            UnsetCurrentItem();

            if(current_place_mode == "object")
            {
                ObjectToolbar.Activate();
                SpawnerToolbar.Deactivate();
                TargetToolbar.Deactivate();
            }
            else if(current_place_mode == "target")
            {
                ObjectToolbar.Deactivate();
                SpawnerToolbar.Deactivate();
                TargetToolbar.Activate();
            }
            else
            {
                ObjectToolbar.Deactivate();
                SpawnerToolbar.Activate();
                TargetToolbar.Deactivate();
            }
        }
    }


    public void ToggleXSymmetry()
    {
        x_symmetry = !x_symmetry;
    }


    public void ToggleYSymmetry()
    {
        y_symmetry = !y_symmetry;
    }


    public void ShowHideObjectObjects(bool should_show)
    {
        if(should_show)
        {
            ShowObjectObjects();
        }
        else
        {
            HideObjectObjects();
        }
    }


    public void ShowHideTargetObjects(bool should_show)
    {
        if(should_show)
        {
            ShowTargetObjects();
        }
        else
        {
            HideTargetObjects();
        }
    }


    public void ShowHideSpawnObjects(bool should_show)
    {
        if(should_show)
        {
            ShowSpawnObjects();
        }
        else
        {
            HideSpawnObjects();
        }
    }


    public void HideObjectObjects()
    {
        HideAll(object_objects);
    }


    public void HideSpawnObjects()
    {
        HideAll(spawn_objects);
    }


    public void HideTargetObjects()
    {
        HideAll(target_objects);
    }


    public void ShowObjectObjects()
    {
        ShowAll(object_objects);
    }


    public void ShowSpawnObjects()
    {
        ShowAll(spawn_objects);
    }


    public void ShowTargetObjects()
    {
        ShowAll(target_objects);
    }


    public void ShowAll(List<GameObject> game_obj_list)
    {
        foreach(GameObject obj in game_obj_list)
        {
            if(obj != null)
            {
                obj.SetActive(true);
            }
        }
    }


    public void HideAll(List<GameObject> game_obj_list)
    {
        foreach(GameObject obj in game_obj_list)
        {
            if(obj != null)
            {
                obj.SetActive(false);
            }
        }
    }


    private void HandleInput()
    {
        HandleCameraInput();
        HandleToolBarSwitching();

        if(InputManager.WasJustPressed(BTNINPUT.toggle_debug))
        {
            Debug.Log(JsonUtility.ToJson(level_data));
        }

        if(InputManager.WasJustPressed(BTNINPUT.pause))
        {
            ExitClicked();
        }
    }


    private void HandleToolBarSwitching()
    {
        if(InputManager.IsDown(BTNINPUT.stop))
        {
            if(InputManager.WasJustPressed(BTNINPUT.hotkey1))
            {
                SwitchMode("object");
            }
            else if(InputManager.WasJustPressed(BTNINPUT.hotkey2))
            {
                SwitchMode("spawn");
            }
            else if(InputManager.WasJustPressed(BTNINPUT.hotkey3))
            {
                SwitchMode("target");
            }
        }
    }


    private void HandleCameraInput()
    {
        float speed = CAMERA_SPEED_SLOW;
        if(InputManager.IsDown(BTNINPUT.stop))
        {
            speed = CAMERA_SPEED_FAST;
        }

        speed *= Time.deltaTime;

        float adjust_x = 0f;
        float adjust_y = 0f;
        float adjust_z = 0f;

        if(InputManager.IsDown(BTNINPUT.up))
        {
            adjust_z = speed;
        }
        else if(InputManager.IsDown(BTNINPUT.down))
        {
            adjust_z = -1f * speed;
        }

        if(InputManager.IsDown(BTNINPUT.right))
        {
            adjust_x = speed;
        }
        else if(InputManager.IsDown(BTNINPUT.left))
        {
            adjust_x = -1f * speed;
        }

        if(InputManager.IsDown(BTNINPUT.zoom_in) || (Input.mouseScrollDelta.y > 0))
        {
            adjust_y = -1 * speed;
        }
        else if(InputManager.IsDown(BTNINPUT.zoom_out)|| (Input.mouseScrollDelta.y < 0))
        {
            adjust_y = speed;
        }

        camera_handle.transform.position = new Vector3(camera_handle.transform.position.x + adjust_x,
                                                       camera_handle.transform.position.y + adjust_y,
                                                       camera_handle.transform.position.z + adjust_z);
    }


    private void FindCamera()
    {
        camera_handle = GameObject.Find("Main Camera");
    }


    private string GetLevelFileFromGameManager()
    {
        return GameManager.GetPendingLevel();
    }


    private void Setup()
    {
        object_objects = new List<GameObject>();
        spawn_objects = new List<GameObject>();
        target_objects = new List<GameObject>();
        SetupLevelData();
        floor = SpawnFloor();
        floor.GetComponent<Renderer>().material = Common.LoadMaterial("Materials/black");
        SpawnPlaceholders();
        camera_handle.transform.position = camera_start_position;
        camera_handle.transform.eulerAngles = camera_start_angles;
    }


    private void SetupLevelData()
    {
        bool test_file_exists = JSONParser.HaveTestLevelData();
        bool test_name_exists = JSONParser.HaveTestLevelName();

        if(test_file_exists)
        {
            level_data = JSONParser.GetTestLevelData();

            if(test_name_exists)
            {
                level_file = JSONParser.GetTestLevelName();
                level_name = JSONParser.GetTestLevelName();
            }
            JSONParser.ClearTestLevelFiles();
        }
        else
        {
            level_file = GetLevelFileFromGameManager();
            if(level_file == null)
            {
                new_level_data = true;
                SetupNewLevelData();
            }
            else
            {
                level_data = ParseLevelData(level_file);
                level_name = Common.GetLevelNameFromFilePath(level_file);
            }
        }
    }


    private void SetupNewLevelData()
    {
        level_data = new LevelData();
        level_data.level_width = GameConfig.DEFAULT_LEVEL_WIDTH;
        level_data.level_height = GameConfig.DEFAULT_LEVEL_HEIGHT;
        level_data.layout = new int[level_data.level_width * level_data.level_height];
        for(int i = 0; i < level_data.layout.Length; i++)
        {
            level_data.layout[i] = LevelData.EMPTY;
        }
    }


    private void SpawnPlaceholders()
    {
        Debug.Log("Spawning placeholders");
        for(int y = (level_data.level_height - 1); y >= 0; y--)
        {
            for(int x = 0; x < level_data.level_width; x++)
            {
                Vector3 spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(x, y, 1f);
                GameObject placeholder_game_obj = Common.Spawn("Placeholder", spawn_pos);
                Placeholder placeholder = placeholder_game_obj.GetComponent<Placeholder>();
                placeholder.Setup(x, y, this);

                if(!new_level_data)
                {
                    int space_level_data = level_data.GetData(x, y);
                    if(space_level_data != LevelData.EMPTY)
                    {
                        PlaceObject(placeholder, space_level_data, GetPrefabName(space_level_data));
                    }

                    if(level_data.spawn_x == x && level_data.spawn_y == y)
                    {
                        PlaceSpawn(placeholder, "player_spawn", "PlayerSpawn");
                    }
                    else if(level_data.blinky_start_x == x && level_data.blinky_start_y == y)
                    {
                        PlaceSpawn(placeholder, "blinky_spawn", "BlinkySpawn");
                    }
                    else if(level_data.pinky_start_x == x && level_data.pinky_start_y == y)
                    {
                        PlaceSpawn(placeholder, "pinky_spawn", "PinkySpawn");
                    }
                    else if(level_data.inky_start_x == x && level_data.inky_start_y == y)
                    {
                        PlaceSpawn(placeholder, "inky_spawn", "InkySpawn");
                    }
                    else if(level_data.clyde_start_x == x && level_data.clyde_start_y == y)
                    {
                        PlaceSpawn(placeholder, "clyde_spawn", "ClydeSpawn");
                    }

                    if(level_data.ghost_house_exit_x == x && level_data.ghost_house_exit_y == y)
                    {
                        PlaceTarget(placeholder, "ghost_exit", "GhostExit");
                    }
                    else if(level_data.eaten_recovery_x == x && level_data.eaten_recovery_y == y)
                    {
                        PlaceTarget(placeholder, "eaten_recovery", "EatenRecovery");
                    }
                    else if(level_data.blinky_scatter_x == x && level_data.blinky_scatter_y == y)
                    {
                        PlaceTarget(placeholder, "blinky_scatter", "BlinkyScatter");
                    }
                    else if(level_data.pinky_scatter_x == x && level_data.pinky_scatter_y == y)
                    {
                        PlaceTarget(placeholder, "pinky_scatter", "PinkyScatter");
                    }
                    else if(level_data.inky_scatter_x == x && level_data.inky_scatter_y == y)
                    {
                        PlaceTarget(placeholder, "inky_scatter", "InkyScatter");
                    }
                    else if(level_data.clyde_scatter_x == x && level_data.clyde_scatter_y == y)
                    {
                        PlaceTarget(placeholder, "clyde_scatter", "ClydeScatter");
                    }
                }
            }
        }
    }
}
