﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public static class JSONParser
{
    public static T ParseJSONFile<T>(string file_path)
    {
        Debug.Log("Loading file as JSON: "+ file_path);
        return ParseJSON<T>(ReadFile(file_path));
    }


    public static LevelData ParseLevelData(string file_path)
    {
        return ParseJSONFile<LevelData>(file_path);
    }


    public static string ReadFile(string file_path)
    {
        return File.ReadAllText(file_path);
    }


    public static void ClearTestLevelFiles()
    {
        string test_level_file_path = Path.Combine(GetCustomLevelDir(), GameConfig.TEST_LEVEL_FILE);
        string test_level_name_file_path = Path.Combine(GetCustomLevelDir(), GameConfig.TEST_LEVEL_FILE_NAME_FILE);
        if(FileExists(test_level_file_path))
        {
            DeleteFile(test_level_file_path);
        }
        if(FileExists(test_level_name_file_path))
        {
            DeleteFile(test_level_name_file_path);
        }
    }


    public static bool HaveTestLevelData()
    {
        string file_path = Path.Combine(GetCustomLevelDir(), GameConfig.TEST_LEVEL_FILE);
        return FileExists(file_path);
    }


    public static bool HaveTestLevelName()
    {
        string file_path = Path.Combine(GetCustomLevelDir(), GameConfig.TEST_LEVEL_FILE_NAME_FILE);
        return FileExists(file_path);
    }


    public static LevelData GetTestLevelData()
    {
        string file_path = Path.Combine(GetCustomLevelDir(), GameConfig.TEST_LEVEL_FILE);
        return ParseLevelData(file_path);
    }


    public static string GetTestLevelName()
    {
        string file_path = Path.Combine(GetCustomLevelDir(), GameConfig.TEST_LEVEL_FILE_NAME_FILE);
        string file_contents = ReadFile(file_path);
        return Common.GetLevelNameFromFilePath(file_contents.Replace("\n", ""));
    }


    public static void WriteTestLevelData(LevelData level_data)
    {
        string file_path = Path.Combine(GetCustomLevelDir(), GameConfig.TEST_LEVEL_FILE);
        WriteJSONFile<LevelData>(file_path, level_data);        
    }


    public static void WriteTestLevelName(string level_file_name)
    {
        string file_path = Path.Combine(GetCustomLevelDir(), GameConfig.TEST_LEVEL_FILE_NAME_FILE);
        WriteFile(file_path, Common.GetLevelNameFromFilePath(level_file_name));
    }


    public static string GetCustomLevelDir()
    {
        return Path.Combine(Application.persistentDataPath, GameConfig.CUSTOM_LEVEL_DIR);
    }


    public static void WriteLevelData(string file_name, LevelData data)
    {
        if(!file_name.EndsWith(".json"))
        {
            file_name += ".json";
        }

        string file_path = Path.Combine(GetCustomLevelDir(), file_name);
        WriteJSONFile<LevelData>(file_path, data);
    }


    public static string GetUserDataFilePath()
    {
        return Path.Combine(Application.persistentDataPath, GameConfig.USER_DATA_FILE);
    }


    public static void WriteUserData(UserData data)
    {
        WriteJSONFile<UserData>(GetUserDataFilePath(), data);
    }


    public static UserData GetUserData()
    {
        string user_data_file = GetUserDataFilePath();
        if(!FileExists(user_data_file))
        {
            WriteUserData(new UserData());
        }
        return ParseJSONFile<UserData>(user_data_file);
    }


    public static void WriteJSONFile<T>(string file_path, T structure)
    {
        WriteFile(file_path, JsonUtility.ToJson(structure));
    }


    public static void WriteFile(string file_path, string contents)
    {
        File.WriteAllText(file_path, contents);
    }


    public static T ParseJSON<T>(string raw_json)
    {
        return JsonUtility.FromJson<T>(raw_json);
    }


    private static string AddStreamingAssetsPath(string file_path)
    {
        if(!file_path.StartsWith(Application.streamingAssetsPath))
        {
            file_path = Path.Combine(Application.streamingAssetsPath, file_path);
        }
        return file_path;
    }


    public static bool FileExists(string file_path)
    {
        return File.Exists(file_path);
    }


    public static void DeleteFile(string file_path)
    {
        File.Delete(file_path);
    }


    public static void EnsureCustomLevelDirExists()
    {
        if(!Directory.Exists(GetCustomLevelDir()))
        {
            Directory.CreateDirectory(GetCustomLevelDir());
        }
    }
}
