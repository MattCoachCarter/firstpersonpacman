using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class LevelBest
{
    public string name;
    public int minutes;
    public int seconds;

    public LevelBest(string n, int m, int s)
    {
        name = n;
        minutes = m;
        seconds = s;
    }


    public string GetName()
    {
        return name;
    }


    public string Format()
    {
        string s = minutes.ToString();
        if(minutes < 10)
        {
            s = "0"+ minutes.ToString();
        }

        s += ":";
        if(seconds < 10)
        {
            s += "0";
        }
        
        s += seconds.ToString();
        return s;
    }


    public bool IsTimeBetter(int min, int sec)
    {
        return (min < minutes) || (min == minutes && sec < seconds);
    }
}
