using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BTNINPUT { up, down, left, right, stop, camera_flip, toggle_debug, zoom_in, zoom_out, hotkey0, hotkey1, hotkey2, hotkey3, hotkey4, hotkey5, hotkey6, hotkey7, hotkey8, hotkey9, pause, debug_pellet };
public enum INPUTSTATE { up, down, just_pressed, held, double_tapped };

public class InputManager : MonoBehaviour
{
    public static InputManager instance;
    private List<Dictionary<BTNINPUT, List<INPUTSTATE>>> input_state_list = null; // 0 = newest > N oldest
    private List<BTNINPUT> all_btn_inputs = new List<BTNINPUT>() {
        BTNINPUT.up,
        BTNINPUT.down,
        BTNINPUT.left,
        BTNINPUT.right,
        BTNINPUT.stop,
        BTNINPUT.camera_flip,
        BTNINPUT.toggle_debug,
        BTNINPUT.zoom_in,
        BTNINPUT.zoom_out,
        BTNINPUT.hotkey0,
        BTNINPUT.hotkey1,
        BTNINPUT.hotkey2,
        BTNINPUT.hotkey3,
        BTNINPUT.hotkey4,
        BTNINPUT.hotkey5,
        BTNINPUT.hotkey6,
        BTNINPUT.hotkey7,
        BTNINPUT.hotkey8,
        BTNINPUT.hotkey9,
        BTNINPUT.pause,
        BTNINPUT.debug_pellet
    };

    // Start is called before the first frame update
    void Start()
    {
        HandleSingleton();
        input_state_list = new List<Dictionary<BTNINPUT, List<INPUTSTATE>>>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();
    }


    public static float GetYaw()
    {
        return GameConfig.Instance.mouse_x_sensitivity * Input.GetAxis("Mouse X");
    }


    public static float GetPitch()
    {
        return GameConfig.Instance.mouse_y_sensitivity * GameConfig.Instance.mouse_invert_y * Input.GetAxis("Mouse Y");
    }


    public static bool WasJustPressed(BTNINPUT btn_input)
    {
        return instance.CheckInputState(btn_input, INPUTSTATE.just_pressed);
    }


    public static bool IsHeld(BTNINPUT btn_input)
    {
        return instance.CheckInputState(btn_input, INPUTSTATE.held);
    }


    public static bool IsDown(BTNINPUT btn_input)
    {
        return instance.CheckInputState(btn_input, INPUTSTATE.down);
    }


    public static bool IsUp(BTNINPUT btn_input)
    {
        return instance.CheckInputState(btn_input, INPUTSTATE.up);
    }


    public static bool WasDoubleTapped(BTNINPUT btn_input)
    {
        return instance.CheckInputState(btn_input, INPUTSTATE.double_tapped);
    }


    private bool CheckInputState(BTNINPUT btn_input, INPUTSTATE input_state)
    {
        return input_state_list.Count > 0 &&
               input_state_list[input_state_list.Count - 1].ContainsKey(btn_input) && 
               input_state_list[input_state_list.Count - 1][btn_input].Contains(input_state);
    }


    private bool HandleSingleton()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if(instance != this)
        {
            Destroy(gameObject); // Don't spawn, there's already an input manager!
            return false;
        }

        return true;
    }


    private void HandleInput()
    {
        if(!CanHandleInput()) { return; }

        List<KeyCode> pressed_keys = FetchKeys();
        List<BTNINPUT> down_inputs = new List<BTNINPUT>();
        List<BTNINPUT> up_inputs = new List<BTNINPUT>(all_btn_inputs);
        for(int i = 0; i < pressed_keys.Count; i++)
        {
            if(GameConfig.CONTROL_CONFIG.ContainsKey(pressed_keys[i]))
            {
                BTNINPUT btn_input = GameConfig.CONTROL_CONFIG[pressed_keys[i]];
                if(!down_inputs.Contains(btn_input))
                {
                    down_inputs.Add(btn_input);
                    up_inputs.Remove(btn_input);
                }
            }
        }

        ComputeInputState(up_inputs, down_inputs);
    }


    private void ComputeInputState(List<BTNINPUT> up_inputs, List<BTNINPUT> down_inputs)
    {
        // input_state_list;
        Dictionary<BTNINPUT, List<INPUTSTATE>> input_state_map = new Dictionary<BTNINPUT, List<INPUTSTATE>>();
        int i;

        for(i = 0; i < up_inputs.Count; i++)
        {
            input_state_map.Add(up_inputs[i], new List<INPUTSTATE>() { INPUTSTATE.up });
        }

        for(i = 0; i < down_inputs.Count; i++)
        {
            List<INPUTSTATE> input_states = new List<INPUTSTATE> { INPUTSTATE.down };

            if(input_state_list.Count > 0)
            {
                // See if the input was just pressed
                if(input_state_list[input_state_list.Count - 1][down_inputs[i]].Contains(INPUTSTATE.up))
                {
                    input_states.Add(INPUTSTATE.just_pressed);

                    // See if the input was just double tapped
                    int frames_since_last_press = 0;
                    for(int j = (input_state_list.Count - 2); j >= 0; j--)
                    {
                        if(input_state_list[j][down_inputs[i]].Contains(INPUTSTATE.just_pressed))
                        {
                            break;
                        }
                        frames_since_last_press++;
                    }

                    if(frames_since_last_press >= GameConfig.BTN_DOUBLE_TAP_MIN_FRAME_GAP && frames_since_last_press <= GameConfig.BTN_DOUBLE_TAP_MAX_FRAME_GAP)
                    {
                        input_states.Add(INPUTSTATE.double_tapped);
                    }
                }
                else
                {
                    // Input is being held down
                    input_states.Add(INPUTSTATE.held);
                }
            }

            input_state_map.Add(down_inputs[i], input_states);
        }

        // Make sure we don't keep too much history
        while(input_state_list.Count >= GameConfig.MAX_INPUT_HISTORY)
        {
            input_state_list.RemoveAt(0);
        }

        // Add the current state to the list of states
        input_state_list.Add(input_state_map);
    }


    private bool CanHandleInput()
    {
        return GameConfig.CONTROL_CONFIG != null;
    }


    private List<KeyCode> FetchKeys()
    {
        List<KeyCode> pressed_keys = new List<KeyCode>();
        for(int i = 0; i < System.Enum.GetNames(typeof(KeyCode)).Length; i++)
        {
            if(Input.GetKey((KeyCode)i))
            {
                pressed_keys.Add((KeyCode)i);
            }
        }

        return pressed_keys;
    }
}
