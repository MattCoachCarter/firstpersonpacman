using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class UserData
{
    public List<LevelBest> default_level_bests;
    public List<LevelBest> custom_level_bests;


    public UserData()
    {
        default_level_bests = null;
        custom_level_bests = null;
    }


    public bool UpdateLevelBest(string name, float level_time, bool is_custom)
    {
        bool updated = false;

        int minutes = (int)Mathf.Floor(level_time / 60f);
        int seconds = ((int)Mathf.Floor(level_time)) - (minutes * 60);

        List<LevelBest> search_list = default_level_bests;
        if(is_custom)
        {
            search_list = custom_level_bests;
        }

        int existing_index = -1;
        for(int i = 0; i < search_list.Count; i++)
        {
            if(search_list[i].GetName() == name)
            {
                existing_index = i;
                break;
            }
        }

        if(existing_index == -1)
        {
            search_list.Add(new LevelBest(name, minutes, seconds));
            updated = true;
        }
        else if(search_list[existing_index].IsTimeBetter(minutes, seconds))
        {
            search_list[existing_index] = new LevelBest(name, minutes, seconds);
            updated = true;
        }

        return updated;
    }
}
