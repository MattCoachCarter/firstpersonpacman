using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : LevelBase
{
    private string state = "settingup";
    public static LevelManager instance = null;
    private GameObject first_portal_seen = null;
    private float level_timer = 0f;
    private bool ever_started = false;
    private GSTATE next_ghost_event = GSTATE.none;
    private float next_ghost_event_time = 0f;
    private float frightened_timer = 0f;
    private List<Ghost> ghosts = null;
    private bool released_pinky = false;
    private bool released_inky = false;
    private bool released_clyde = false;
    private GSTATE target_ghost_state = GSTATE.scatter;
    private int pellets_required = 0;
    private int pellets_consumed = 0;
    private int chase_sequences = 0;
    private List<List<GameObject>> pellet_map = null;
    private List<GameObject> all_spawned_objects = null;
    private List<GameObject> destroy_on_start_over = null;
    private float level_lose_timer = -1f;
    private GameObject floor = null;
    private GameObject ceiling = null;
    private float ghost_release_cooldown_timer = GameConfig.GHOST_RELEASE_COOLDOWN;
    private List<GameObject> gates = null;
    private bool test_mode = false;
    private bool paused = false;
    private GameObject camera_handle = null;
    private Vector3 camera_move_target;
    private float camera_sweep_starting_distance;
    private const float CAMERA_SWEEP_SPEED = 10f;
    private const float CAMERA_SWEEP_ROT_SPEED = 150f;
    private Vector3 camera_sweep_rotate_target = new Vector3(0, -90, 0);
    private float start_cooldown = 0f;
    private List<Vector2> spawned_coords = null;
    private float time_since_first_start = 0f;
    private int attempts = 0;
    private string level_file = null;


    // Start is called before the first frame update
    void Start()
    {
        camera_handle = GameObject.Find("Main Camera");
        instance = this;
        test_mode = GameManager.IsInLevelTestMode();
        if(test_mode)
        {
            level_data = GameManager.GetLevelTestData();
            Setup();
        }
        else
        {
            Setup(GameManager.GetPendingLevel());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(ever_started && state != "won" && state != "showingwinscreen")
        {
            time_since_first_start += Time.deltaTime;
        }

        if(state == "playing")
        {
            HandleTimerEvents();
            HandlePauseInput();
            if(InputManager.IsDown(BTNINPUT.debug_pellet))
            {
                PelletConsumed();
            }
        }
        else if(state == "sweepingcamera")
        {
            LoadingSplashScreen.Hide();
            SweepCamera();
        }
        else if(state == "starting")
        {
            LoadingSplashScreen.Hide();
            start_cooldown = GameConfig.LEVEL_START_DELAY;
            state = "startdelay";
            Player.TakeControlOfCamera();
            ReadyDisplay.Activate();
        }
        else if(state == "startdelay")
        {
            start_cooldown -= Time.deltaTime;
            if(start_cooldown <= 0f && RunMovers())
            {
                ReadyDisplay.Deactivate();
                LetterboxHandler.Unletterbox();
                state = "playing";
            }
        }
        else if(state == "levellost")
        {
            if(level_lose_timer > 0f)
            {
                level_lose_timer -= Time.deltaTime;
                if(level_lose_timer <= 0f)
                {
                    level_lose_timer = -1f;
                    StartLevel();
                }
            }
        }
        else if(state == "won")
        {
            Time.timeScale = 0;
            if(level_file != null)
            {
                string level_name = Common.GetLevelNameFromFilePath(level_file);
                UserData user_data = GameManager.GetUserData();
                bool is_custom_level = GameManager.GetInCustomLevel();
                if(user_data.UpdateLevelBest(level_name, time_since_first_start, is_custom_level))
                {
                    GameManager.WriteUserData();
                }
            }

            WinScreen.Activate(time_since_first_start, (attempts - 1));
            state = "showingwinscreen";
        }
    }


    private void HandlePauseInput()
    {
        if(InputManager.WasJustPressed(BTNINPUT.pause))
        {
            if(paused)
            {
                Unpause();
            }
            else
            {
                Pause();
            }
        }
    }


    public static void Pause()
    {
        PauseMenu.Activate(instance.test_mode);
        Time.timeScale = 0;
        instance.paused = true;
    }


    public static void Unpause()
    {
        PauseMenu.Deactivate();
        Time.timeScale = 1;
        instance.paused = false;
    }


    public GSTATE GetTargetGhostState()
    {
        return target_ghost_state;
    }


    public float GetFrightenedTimer()
    {
        return frightened_timer;
    }


    private void HandleTimerEvents()
    {
        level_timer += Time.deltaTime;
        ghost_release_cooldown_timer = Common.ApproachZero(ghost_release_cooldown_timer, Time.deltaTime);

        if(frightened_timer > 0f)
        {
            frightened_timer -= Time.deltaTime;
            GhostIndicator.instance.GhostUpdate();
            if(frightened_timer <= 0f)
            {
                UnfrightenGhosts();
            }
        }

        if(ghost_release_cooldown_timer <= 0f)
        {
            if(!released_pinky)
            {
                ReleaseGhost();
                released_pinky = true;
                ResetGhostReleaseCooldown();
            }
            else if(ShouldReleaseInky())
            {
                ReleaseGhost();
                released_inky = true;
                ResetGhostReleaseCooldown();
            }
            else if(ShouldReleaseClyde())
            {
                ReleaseGhost();
                released_clyde = true;
                ResetGhostReleaseCooldown();
            }
        }

        if(level_timer >= next_ghost_event_time)
        {
            DoGhostEvent(next_ghost_event);
            if(next_ghost_event == GSTATE.chase)
            {
                float extra_time = (((float)chase_sequences) * (GameConfig.BASE_CHASE_TIME / 2f));
                float chase_time = GameConfig.BASE_CHASE_TIME + extra_time;
                next_ghost_event = GSTATE.scatter;
                next_ghost_event_time = level_timer + chase_time;
                Debug.Log(chase_sequences +"th chase sequence: "+ chase_time +" sec ("+ extra_time +" was extra)");
                chase_sequences++;
            }
            else
            {
                next_ghost_event = GSTATE.chase;
                next_ghost_event_time = level_timer + GameConfig.BASE_SCATTER_TIME;
            }
        }
    }


    public void SetDataForLevelStart()
    {
        level_lose_timer = -1f;
        target_ghost_state = GSTATE.scatter;
        ever_started = true;
        level_timer = 0;
        chase_sequences = 0;
        next_ghost_event = GSTATE.chase;
        next_ghost_event_time = 7f;
        if(GhostIndicator.instance != null)
        {
            GhostIndicator.instance.Reset();
        }
        if(Minimap.instance != null)
        {
            Minimap.instance.DoReset();
        }
        level_timer = 0f;
        frightened_timer = 0f;
        ResetGhostReleaseCooldown();
        released_pinky = false;
        released_inky = false;
        released_clyde = false;
        state = "playing";
    }


    private void ResetGhostReleaseCooldown()
    {
        ghost_release_cooldown_timer = GameConfig.GHOST_RELEASE_COOLDOWN;
    }


    public void FirstTimeStart()
    {
        SetDataForLevelStart();
        if(PelletStatus.instance != null)
        {
            PelletStatus.instance.Reset();
        }
    }


    private void ContinueStart()
    {
        foreach(GameObject obj in destroy_on_start_over)
        {
            Common.SafeDestroy(obj);
        }
        ghosts = null;

        SetDataForLevelStart();

        EnableCeiling();
        SpawnPlayer();
        SpawnGhosts();
    }


    public void StartLevel()
    {
        attempts++;
        LetterboxHandler.Letterbox();
        if(ever_started)
        {
            ContinueStart();
            state = "starting";
        }
        else
        {
            FirstTimeStart();
            state = "sweepingcamera";
        }
    }


    private bool RunMovers()
    {
        if(Player.instance != null)
        {
            Debug.Log("Running movers");
            Player.Run();
            foreach(Ghost g in ghosts)
            {
                g.Run();
            }
            return true;
        }
        return false;
    }


    private bool UnrunMovers()
    {
        if(Player.instance != null)
        {
            Debug.Log("Unrunning movers");
            Player.Unrun();
            foreach(Ghost g in ghosts)
            {
                g.Unrun();
            }
            return true;
        }
        return false;
    }


    private void DoGhostEvent(GSTATE event_name)
    {
        if(event_name == GSTATE.chase)
        {
            NotificationHandler.instance.Notify("chase!");
        }
        else if(event_name == GSTATE.scatter)
        {
            NotificationHandler.instance.Notify("scatter!");
        }
        
        GhostStateChange(event_name);
    }


    private void ReleaseGhost()
    {
        for(int i = 1; i < ghosts.Count; i++)
        {
            if(ghosts[i].GetState() == GSTATE.waiting)
            {
                ghosts[i].ChangeState(GSTATE.release);
                break;
            }
        }
    }


    public List<Ghost> GetGhosts()
    {
        return ghosts;
    }


    public LevelData GetLevelData()
    {
        return level_data;
    }


    private void GhostStateChange(GSTATE new_state)
    {
        GhostStateChange(new_state, false);
    }


    private int GetFrightenTimePercentageLeft()
    {
        return (int)Mathf.Round((GetFrightenedTimer() / GetFrightenTime()) * 100f);
    }


    public bool ShouldFrightenFlashWhite()
    {
        int frighten_time_percentage_left = GetFrightenTimePercentageLeft();
        return (frighten_time_percentage_left <= 25 && frighten_time_percentage_left > 20) ||
               (frighten_time_percentage_left <= 15 && frighten_time_percentage_left > 10) ||
               (frighten_time_percentage_left <= 5);
    }


    private void GhostStateChange(GSTATE new_state, bool frightened_override)
    {
        Debug.Log("GHOST STATE: "+ new_state);

        if(new_state != GSTATE.frightened)
        {
            target_ghost_state = new_state;
        }

        foreach(Ghost ghost in ghosts)
        {
            if(ghost.GetState() != GSTATE.waiting &&
               ghost.GetState() != GSTATE.release &&
               ghost.GetState() != GSTATE.eaten &&
               ghost.GetState() != GSTATE.pause &&
               (ghost.GetState() != GSTATE.frightened || frightened_override))
            {
                ghost.ChangeState(new_state);
            }
        }
    }


    private void SpawnByLevelDataConst(int level_data_const, int x, int y)
    {
        Vector2 spawn_level_coords = new Vector2(x, y);
        if(spawned_coords.Contains(spawn_level_coords))
        {
            return;
        }

        string prefab_name = GetPrefabName(level_data_const);
        if(prefab_name != null)
        {
            Vector3 spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(x, y, GetSpawnY(level_data_const));
            int x_scale = 1;
            int z_scale = 1;
            bool stitching = false;

            if(LevelData.IsSolid(level_data_const))
            {
                // We need to spawn in stitching mode
                // Check for horizontal stitching
                bool keep_going = true;
                int objects_to_stitch = 0;
                while(keep_going)
                {
                    keep_going = false;
                    int new_x = objects_to_stitch + 1 + x;
                    int next_data = level_data.GetData(new_x, y);
                    if(next_data == level_data_const && !spawned_coords.Contains(new Vector2(new_x, y)))
                    {
                        keep_going = true;
                        objects_to_stitch++;
                        spawned_coords.Add(new Vector2(new_x, y));
                    }
                }

                if(objects_to_stitch > 0)
                {
                    // Stitching horizontally
                    // Try to stitch in a block vertically as well if possible
                    int vert_stitch = 0;
                    keep_going = true;
                    while(keep_going)
                    {
                        keep_going = false;
                        bool all_match = true;
                        for(int i = 0; i < objects_to_stitch; i++)
                        {
                            int new_x = i + 1 + x;
                            int new_y = y + vert_stitch + 1;
                            int next_data = level_data.GetData(new_x, new_y);
                            if(next_data != level_data_const || spawned_coords.Contains(new Vector2(new_x, new_y)))
                            {
                                all_match = false;
                                break;
                            }
                        }

                        if(all_match)
                        {
                            keep_going = true;
                            for(int i = 0; i < objects_to_stitch; i++)
                            {
                                int new_x = i + 1 + x;
                                int new_y = y + vert_stitch + 1;
                                spawned_coords.Add(new Vector2(new_x, new_y));
                            }
                            vert_stitch++;
                        }
                    }

                    float new_x_spawn_pos = (((float)x) + (((float)objects_to_stitch) / 2f));
                    float new_y_spawn_pos = (float)y + (((float)vert_stitch) / 2f);
                    spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(new_x_spawn_pos, new_y_spawn_pos, GetSpawnY(level_data_const));
                    x_scale = 1 + objects_to_stitch;
                    z_scale = 1 + vert_stitch;
                }
                else
                {
                    // Check for vertical stitching
                    keep_going = true;
                    objects_to_stitch = 0;
                    while(keep_going)
                    {
                        keep_going = false;
                        int new_y = objects_to_stitch + 1 + y;
                        int next_data = level_data.GetData(x, new_y);
                        if(next_data == level_data_const && !spawned_coords.Contains(new Vector2(x, new_y)))
                        {
                            keep_going = true;
                            objects_to_stitch++;
                            spawned_coords.Add(new Vector2(x, new_y));
                        }
                    }

                    if(objects_to_stitch > 0)
                    {
                        float new_x_spawn_pos = (float)x;
                        float new_y_spawn_pos = (((float)y) + (((float)objects_to_stitch) / 2f));
                        spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(new_x_spawn_pos, new_y_spawn_pos, GetSpawnY(level_data_const));
                        z_scale = 1 + objects_to_stitch;
                    }
                }

                stitching = objects_to_stitch > 0;
            }

            GameObject spawned_game_obj = Common.Spawn(prefab_name, spawn_pos);

            if(stitching)
            {
                float new_x_scale = spawned_game_obj.transform.localScale.x * x_scale;
                float new_z_scale = spawned_game_obj.transform.localScale.z * z_scale;
                float new_collider_size_x = (GameConfig.SOLID_COLLIDER_OVERSIZE_TARGET + new_x_scale) / new_x_scale;
                float new_collider_size_z = (GameConfig.SOLID_COLLIDER_OVERSIZE_TARGET + new_z_scale) / new_z_scale;
                spawned_game_obj.transform.localScale = new Vector3(new_x_scale,
                                                                    spawned_game_obj.transform.localScale.y,
                                                                    new_z_scale);
                BoxCollider box_collider = spawned_game_obj.GetComponent<BoxCollider>();
                box_collider.size = new Vector3(new_collider_size_x,
                                                box_collider.size.y,
                                                new_collider_size_z);
            }


            if(prefab_name == "Portal")
            {
                if(first_portal_seen == null)
                {
                    first_portal_seen = spawned_game_obj;
                }
                else
                {
                    spawned_game_obj.GetComponent<Portal>().link = first_portal_seen;
                    first_portal_seen.GetComponent<Portal>().link = spawned_game_obj;
                }
            }

            if(prefab_name == "Gate")
            {
                gates.Add(spawned_game_obj);
            }

            if(level_data_const == LevelData.PORTAL || level_data_const == LevelData.GATE)
            {
                // These might have to be rotated, see if there are walls above and below
                int data_above = level_data.GetData(x, y + 1);
                int data_below = level_data.GetData(x, y - 1);
                if((data_above == LevelData.WALL || data_above == LevelData.TALL_WALL || data_above == LevelData.GATE) &&
                   (data_below == LevelData.WALL || data_below == LevelData.TALL_WALL || data_below == LevelData.GATE))
                {
                    spawned_game_obj.transform.eulerAngles = new Vector3(0f, 90f, 0f);
                }
            }

            if(level_data_const == LevelData.PELLET || level_data_const == LevelData.POWER_PELLET)
            {
                pellet_map[x][y] = spawned_game_obj;
                pellets_required++;
            }

            all_spawned_objects.Add(spawned_game_obj);
        }
    }


    public bool DoesPelletExistAt(int x, int y)
    {
        if(x < 0 || y < 0 || x >= pellet_map.Count || y >= pellet_map[x].Count)
        {
            return false;
        }
        return pellet_map[x][y] != null;
    }


    public GameObject GetFloor()
    {
        return floor;
    }


    private void SpawnObjects()
    {
        spawned_coords = new List<Vector2>();
        gates = new List<GameObject>();
        all_spawned_objects = new List<GameObject>();
        destroy_on_start_over = new List<GameObject>();

        pellet_map = new List<List<GameObject>>();
        for(int x = 0; x < level_data.level_width; x++)
        {
            pellet_map.Add(new List<GameObject>());
            for(int y = 0; y < level_data.level_height; y++)
            {
                pellet_map[x].Add(null);
            }
        }

        SpawnPlayer();

        floor = SpawnFloor();
        ceiling = SpawnCeiling();
        all_spawned_objects.Add(floor);
        all_spawned_objects.Add(ceiling);
        
        for(int x = 0; x < level_data.level_width; x++)
        {
            for(int y = 0; y < level_data.level_height; y++)
            {
                SpawnByLevelDataConst(level_data.GetData(x, y), x, y);
            }
        }

        SpawnGhosts();
    }


    private void SpawnPlayer()
    {
        Vector3 spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(level_data.spawn_x,
                                                                   level_data.spawn_y,
                                                                   GameConfig.PLAYER_SPAWN_HEIGHT);
        destroy_on_start_over.Add(Common.Spawn("Player", spawn_pos, 270f));
    }


    private void SpawnGhosts()
    {
        ghosts = new List<Ghost>();

        GameObject blinky_game_object = SpawnBlinky();
        GameObject pinky_game_object = SpawnPinky();
        GameObject inky_game_object = SpawnInky();
        GameObject clyde_game_object = SpawnClyde();

        Collider blinky_collider = blinky_game_object.GetComponent<Collider>();
        Collider pinky_collider = pinky_game_object.GetComponent<Collider>();
        Collider inky_collider = inky_game_object.GetComponent<Collider>();
        Collider clyde_collider = clyde_game_object.GetComponent<Collider>();

        Physics.IgnoreCollision(blinky_collider, pinky_collider, true);
        Physics.IgnoreCollision(blinky_collider, inky_collider, true);
        Physics.IgnoreCollision(blinky_collider, clyde_collider, true);
        Physics.IgnoreCollision(pinky_collider, inky_collider, true);
        Physics.IgnoreCollision(pinky_collider, clyde_collider, true);
        Physics.IgnoreCollision(inky_collider, clyde_collider, true);

        foreach(GameObject gate_obj in gates)
        {
            Collider gate_collider = gate_obj.GetComponent<Collider>();
            Physics.IgnoreCollision(blinky_collider, gate_collider, true);
            Physics.IgnoreCollision(pinky_collider, gate_collider, true);
            Physics.IgnoreCollision(inky_collider, gate_collider, true);
            Physics.IgnoreCollision(clyde_collider, gate_collider, true);
         
        }

        destroy_on_start_over.Add(blinky_game_object);
        destroy_on_start_over.Add(pinky_game_object);
        destroy_on_start_over.Add(inky_game_object);
        destroy_on_start_over.Add(clyde_game_object);
    }


    private GameObject SpawnBlinky()
    {
        Vector3 spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(level_data.blinky_start_x,
                                                                   level_data.blinky_start_y,
                                                                   GameConfig.GHOST_SPAWN_HEIGHT);
        GameObject ghost_game_object = Common.Spawn("Ghost", spawn_pos);
        Ghost ghost = ghost_game_object.GetComponent<Ghost>();
        ghost.Setup("blinky", level_data);
        ghosts.Add(ghost);
        return ghost_game_object;
    }


    private GameObject SpawnPinky()
    {
        Vector3 spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(level_data.pinky_start_x,
                                                                   level_data.pinky_start_y,
                                                                   GameConfig.GHOST_SPAWN_HEIGHT);
        GameObject ghost_game_object = Common.Spawn("Ghost", spawn_pos);
        Ghost ghost = ghost_game_object.GetComponent<Ghost>();
        ghost.Setup("pinky", level_data);
        ghosts.Add(ghost);
        return ghost_game_object;
    }


    private GameObject SpawnInky()
    {
        Vector3 spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(level_data.inky_start_x,
                                                                   level_data.inky_start_y,
                                                                   GameConfig.GHOST_SPAWN_HEIGHT);
        GameObject ghost_game_object = Common.Spawn("Ghost", spawn_pos);
        Ghost ghost = ghost_game_object.GetComponent<Ghost>();
        ghost.Setup("inky", level_data);
        ghosts.Add(ghost);
        return ghost_game_object;
    }


    private GameObject SpawnClyde()
    {
        Vector3 spawn_pos = level_data.GetBottomLeftCoordOfLevelXY(level_data.clyde_start_x,
                                                                   level_data.clyde_start_y,
                                                                   GameConfig.GHOST_SPAWN_HEIGHT);
        GameObject ghost_game_object = Common.Spawn("Ghost", spawn_pos);
        Ghost ghost = ghost_game_object.GetComponent<Ghost>();
        ghost.Setup("clyde", level_data);
        ghosts.Add(ghost);
        return ghost_game_object;
    }


    public void DoLevelLose(GameObject ghost_object_responsible)
    {
        PauseAllGhosts();
        DisableCeiling();
        Player.instance.DoLose(ghost_object_responsible);
        level_lose_timer = GameConfig.LEVEL_LOSE_DELAY;
        state = "levellost";
    }


    private void PauseAllGhosts()
    {
        foreach(Ghost ghost in ghosts)
        {
            ghost.ChangeState(GSTATE.pause);
        }
    }


    private GameObject SpawnCeiling()
    {
        return SpawnFloor(31f);
    }


    public void Setup(string l_file)
    {
        level_file = l_file;
        level_data = ParseLevelData(l_file);
        Setup();
    }


    public void Setup()
    {
        SpawnObjects();
        SetCameraStartPosition();
        StartLevel();
    }


    private void SetCameraStartPosition()
    {
        int start_x = (level_data.spawn_x + 1);
        int start_y = (level_data.spawn_y - 6);
        int target_x = (level_data.spawn_x + 1);
        int target_y = level_data.spawn_y;
        float camera_target_y = (GameConfig.PLAYER_SPAWN_HEIGHT + GameConfig.CAMERA_PLAYER_OFFSET.y);
        Vector3 camera_start_pos = level_data.GetBottomLeftCoordOfLevelXY(start_x, start_y, GameConfig.LEVEL_CAMERA_SWEEP_Y_START);
        Vector3 camera_start_rot = new Vector3(70, 0, 0);
        camera_handle.transform.position = camera_start_pos;
        camera_handle.transform.eulerAngles = camera_start_rot;
        camera_move_target = level_data.GetBottomLeftCoordOfLevelXY(target_x, target_y, camera_target_y);
        camera_sweep_starting_distance = GetCameraDistanceToTarget();
        DisableCeiling();
    }


    private float GetCameraDistanceToTarget()
    {
        return Vector3.Distance(camera_handle.transform.position,
                                camera_move_target);
    }


    private void SweepCamera()
    {
        float distance_to_target = GetCameraDistanceToTarget();
        float pct_done = 1f - (distance_to_target / camera_sweep_starting_distance);
        
        if(pct_done >= 0.99f)
        {
            state = "starting";
        }
        else
        {
            if(camera_handle.transform.position.y < ceiling.transform.position.y)
            {
                EnableCeiling();
            }

            float easing_modifier = (Mathf.Sin(Mathf.PI * pct_done) + 1f) * 2.5f;

            MoveCameraForSweep(easing_modifier);
            if(pct_done >= 0.9f)
            {
                float camera_target_y = (GameConfig.PLAYER_SPAWN_HEIGHT + GameConfig.CAMERA_PLAYER_OFFSET.y);
                camera_move_target = level_data.GetBottomLeftCoordOfLevelXY(level_data.spawn_x, level_data.spawn_y, camera_target_y);
                RotateCameraForSweep();
            }
        }
    }


    private void MoveCameraForSweep(float easing_modifier)
    {
        float approach_amount = (CAMERA_SWEEP_SPEED * easing_modifier) * Time.deltaTime;
        float new_x = Common.ApproachTarget(camera_handle.transform.position.x, approach_amount, camera_move_target.x);
        float new_y = Common.ApproachTarget(camera_handle.transform.position.y, approach_amount, camera_move_target.y);
        float new_z = Common.ApproachTarget(camera_handle.transform.position.z, approach_amount, camera_move_target.z);

        Vector3 new_pos = new Vector3(new_x, new_y, new_z);
        camera_handle.transform.position = new_pos;
    }


    private void RotateCameraForSweep()
    {
        float approach_amount = CAMERA_SWEEP_ROT_SPEED * Time.deltaTime;
        float new_x = Common.ApproachTarget(camera_handle.transform.eulerAngles.x, approach_amount, camera_sweep_rotate_target.x);
        float new_y = Common.ApproachTarget(camera_handle.transform.eulerAngles.y, approach_amount, camera_sweep_rotate_target.y);
        float new_z = Common.ApproachTarget(camera_handle.transform.eulerAngles.z, approach_amount, camera_sweep_rotate_target.z);

        Vector3 new_pos = new Vector3(new_x, new_y, new_z);
        camera_handle.transform.eulerAngles = new_pos;
    }


    public void PelletConsumed() { PelletConsumed(false); }
    public void PelletConsumed(bool power)
    {
        pellets_consumed++;
        PelletStatus.instance.PctUpdate(pellets_consumed, pellets_required);

        if(power)
        {
            Minimap.instance.PowerPelletCollect();
        }
        else
        {
            Minimap.instance.PelletCollect();
        }

        if(pellets_consumed >= pellets_required)
        {
            state = "won";
        }
    }


    public void PowerPelletConsumed()
    {
        PelletConsumed(true);
        FrightenGhosts();
    }


    public float GetFrightenTime()
    {
        return GameConfig.BASE_FRIGHTEN_TIME - (((float)level_data.difficulty) / 2f);
    }


    private void FrightenGhosts()
    {
        frightened_timer = GetFrightenTime();
        GhostStateChange(GSTATE.frightened);
    }


    private void UnfrightenGhosts()
    {
        GhostStateChange(target_ghost_state, true);
        // TODO: UI Stuff
    }


    public void EnableCeiling()
    {
        ceiling.SetActive(true);
    }


    public void DisableCeiling()
    {
        ceiling.SetActive(false);
    }


    private bool ShouldReleaseInky()
    {
        bool should_release = false;
        if(!released_inky)
        {
            float pellet_release_pct = GameConfig.BASE_PELLET_RELEASE_A + (level_data.difficulty / 100f);
            int release_threshold = (int)Mathf.Round(((float)pellets_required) * pellet_release_pct);
            should_release = pellets_consumed >= release_threshold;
        }

        return should_release;
    }


    public Ghost GetBlinky()
    {
        if(ghosts != null && ghosts.Count > 0)
        {
            return ghosts[0];
        }
        return null;
    }


    private bool ShouldReleaseClyde()
    {
        bool should_release = false;
        if(!released_clyde)
        {
            float pellet_release_pct = GameConfig.BASE_PELLET_RELEASE_B + (level_data.difficulty / 50f);
            int release_threshold = (int)Mathf.Round(((float)pellets_required) * pellet_release_pct);
            should_release = pellets_consumed >= release_threshold;
        }

        return should_release;
    }
}
