using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadyDisplay : MonoBehaviour
{
    public static ReadyDisplay instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public static void Deactivate()
    {
        instance.gameObject.SetActive(false);
    }


    public static void Activate()
    {
        instance.gameObject.SetActive(true);
    }
}
