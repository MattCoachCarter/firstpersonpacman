using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum NOTIFSTYLE { basic, blinky, pinky, inky, clyde };
public enum NOTESTATE { idle, writing, displaying, erasing };


public class NotificationHandler : UIHelper
{
    public static NotificationHandler instance = null;
    private List<string> notification_queue;
    private List<NOTIFSTYLE> style_queue;
    private float notification_timer = 0f;
    private float write_timer = 0f;
    private string current_notif_text = "";
    private int current_notif_text_length = 0;
    private string target_notif_text = "";
    private NOTESTATE state = NOTESTATE.idle;
    private float write_spacing = 0f;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        notification_queue = new List<string>();
        style_queue = new List<NOTIFSTYLE>();
    }

    // Update is called once per frame
    void Update()
    {
        if(notification_timer <= 0f && state != NOTESTATE.erasing)
        {
            if(state == NOTESTATE.displaying)
            {
                state = NOTESTATE.erasing;
                write_spacing /= 2f;
                EraseNext();
            }
            else if(state == NOTESTATE.idle && (notification_queue.Count > 0))
            {
                ClearText();
                ShowNextNotification();
            }
        }
        else
        {
            if(state == NOTESTATE.writing || state == NOTESTATE.erasing)
            {
                if(write_timer <= 0f)
                {
                    if(state == NOTESTATE.erasing)
                    {
                        if(current_notif_text_length > 0)
                        {
                            EraseNext();
                        }
                        else
                        {
                            state = NOTESTATE.idle;
                        }
                    }
                    else
                    {
                        if(current_notif_text_length < target_notif_text.Length)
                        {
                            WriteNext();
                        }
                        else
                        {
                            state = NOTESTATE.displaying;
                        }
                    }
                }
                else
                {
                    write_timer -= Time.deltaTime;
                }
            }
            else
            {
                notification_timer -= Time.deltaTime;
            }
        }
    }


    public void Notify(string notif_txt) { Notify(notif_txt, NOTIFSTYLE.basic); }
    public void Notify(string notif_txt, NOTIFSTYLE style)
    {
        notification_queue.Add(notif_txt);
        style_queue.Add(style);
    }


    private void ShowNextNotification()
    {
        if(notification_queue.Count <= 0)
        {
            state = NOTESTATE.idle;
        }
        else
        {
            string notification_text = notification_queue[0];
            ShowNotification(notification_text, style_queue[0]);
            notification_queue.RemoveAt(0);
            style_queue.RemoveAt(0);
            state = NOTESTATE.writing;
            write_timer = 0f;
            write_spacing = 0.6f / ((float)notification_text.Length);
        }
    }


    private void ShowNotification(string notif_txt, NOTIFSTYLE style)
    {
        notification_timer = GameConfig.NOTIFICATION_TIME;
        current_notif_text = "";
        current_notif_text_length = 0;
        target_notif_text = notif_txt;
        SetChildTextColor("NotificationText", StyleToColor(style));
    }


    private Color StyleToColor(NOTIFSTYLE style)
    {
        if(style == NOTIFSTYLE.blinky)
        {
            return new Color(1f, 0f, 0f, 1f);
        }
        else if(style == NOTIFSTYLE.pinky)
        {
            return new Color(1f, 0.72f, 1f, 1f);
        }
        else if(style == NOTIFSTYLE.inky)
        {
            return new Color(0f, 1f, 1f, 1f);
        }
        else if(style == NOTIFSTYLE.clyde)
        {
            return new Color(1f, 0.79f, 0.32f, 1f);
        }
        
        return new Color(1f, 1f, 0f, 1f);
    }


    private void WriteNext()
    {
        write_timer = write_spacing;
        current_notif_text_length++;
        ComputeSubstrAndDisplay();
    }


    private void EraseNext()
    {
        write_timer = write_spacing;
        current_notif_text_length--;
        ComputeSubstrAndDisplay();
    }


    private void ComputeSubstrAndDisplay()
    {
        current_notif_text = "";

        for(int i = 0; i < target_notif_text.Length; i++)
        {
            if(i < current_notif_text_length || target_notif_text[i].ToString() == " ")
            {
                current_notif_text += target_notif_text[i];
            }
            else
            {
                current_notif_text += "__";
            }
        }

        DisplayCurrentText();
    }


    private void DisplayCurrentText()
    {
        SetText(current_notif_text);
    }


    private void ClearText()
    {
        SetText("");
    }


    private void SetText(string t)
    {
        SetChildText("NotificationText", t);
    }
}
