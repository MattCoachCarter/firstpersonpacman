using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minimap : UIHelper
{
    public static Minimap instance;
    private List<List<Image>> minimap_images = null;
    private LevelData level_data = null;
    private bool ready = false;
    private bool first = true;
    private float time_available = 0f;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Setup();
    }

    // Update is called once per frame
    void Update()
    {
        if(!ready)
        {
            Setup();
        }
        else
        {
            UpdateTimeAvailable();
            if(first)
            {
                DisplayMinimapHider();
                DisplayMinimap();
            }
            else if(IsAvailable())
            {
                HideMinimapHider();
                DisplayMinimap();
            }
            else
            {
                DisplayMinimapHider();
            }
            first = false;
        }
    }


    public void PelletCollect()
    {
        time_available += GameConfig.PELLET_MINIMAP_TIME;
    }


    public void PowerPelletCollect()
    {
        time_available += GameConfig.POWER_PELLET_MINIMAP_TIME;
    }


    private void HideMinimapHider()
    {
        DeactivateChild("MinimapHider");
    }


    private void DisplayMinimapHider()
    {
        ActivateChild("MinimapHider");
    }


    public void DoReset()
    {
        time_available = 0f;
        ready = false;
        Setup();
    }


    private void Setup()
    {
        if(LevelManager.instance != null && Player.instance != null)
        {
            SetupImageGrid();
            level_data = LevelManager.instance.GetLevelData();
            ready = true;
        }
    }


    public bool IsAvailable()
    {
        return time_available > 0f;
    }


    private void UpdateTimeAvailable()
    {
        time_available = Common.ApproachZero(time_available, Time.deltaTime);
        SetChildText("TimerText", time_available.ToString("F1"));
    }


    private Vector2 GetPlayerCurrentXY()
    {
        return Common.ThreeDToLevelCoords(Player.instance.gameObject.transform.position);
    }


    private string LevelDataToMinimapSprite(int level_data_const, int x, int y)
    {
        if(level_data_const == LevelData.PELLET)
        {
            if(LevelManager.instance.DoesPelletExistAt(x, y))
            {
                return "Sprites/Minimap/mini_pellet";
            }
        }
        else if(level_data_const == LevelData.POWER_PELLET)
        {
            if(LevelManager.instance.DoesPelletExistAt(x, y))
            {
                return "Sprites/Minimap/mini_power_pellet";
            }
        }
        else if(level_data_const == LevelData.PORTAL)
        {
            return "Sprites/Minimap/mini_horiz_portal";
        }
        else if(level_data_const == LevelData.WALL)
        {
            return "Sprites/Minimap/mini_wall";
        }
        else if(level_data_const == LevelData.GATE)
        {
            return "Sprites/Minimap/mini_gate";
        }
        else if(level_data_const == LevelData.TALL_WALL)
        {
            return "Sprites/Minimap/mini_wall";
        }

        return "Sprites/Minimap/mini_blank";
    }


    private void DisplayMinimap()
    {
        List<Ghost> ghosts = LevelManager.instance.GetGhosts();
        if(ghosts == null || ghosts.Count != 4)
        {
            return;
        }

        Vector2 player_x_y = GetPlayerCurrentXY();
        Vector2 blinky_x_y = ghosts[0].GetLevelXY();
        Vector2 pinky_x_y = ghosts[1].GetLevelXY();
        Vector2 inky_x_y = ghosts[2].GetLevelXY();
        Vector2 clyde_x_y = ghosts[3].GetLevelXY();

        int half_minimap_width = (int)Mathf.Floor(((float)GameConfig.MINIMAP_WIDTH) / 2f);
        int half_minimap_height = (int)Mathf.Floor(((float)GameConfig.MINIMAP_HEIGHT) / 2f);

        for(int x = 0; x < GameConfig.MINIMAP_WIDTH; x++)
        {
            int level_x = (((int)player_x_y.x) - half_minimap_width) + x;
            for(int y = 0; y < GameConfig.MINIMAP_HEIGHT; y++)
            {
                int level_y = (((int)player_x_y.y) - half_minimap_height) + y;
                Vector2 level_x_y = new Vector2(level_x, level_y);

                string sprite = null;

                if(level_x_y == player_x_y)
                {
                    sprite = "Sprites/Minimap/mini_pacman";
                }
                else if(level_x_y == blinky_x_y)
                {
                    sprite = GetSpriteForGhost(ghosts[0]);
                }
                else if(level_x_y == pinky_x_y)
                {
                    sprite = GetSpriteForGhost(ghosts[1]);
                }
                else if(level_x_y == inky_x_y)
                {
                    sprite = GetSpriteForGhost(ghosts[2]);
                }
                else if(level_x_y == clyde_x_y)
                {
                    sprite = GetSpriteForGhost(ghosts[3]);
                }
                else
                {
                    int x_y_level_data = level_data.GetData(level_x, level_y);
                    sprite = LevelDataToMinimapSprite(x_y_level_data, level_x, level_y);
                }

                minimap_images[x][y].sprite = Common.LoadSprite(sprite);
            }
        }
    }


    private string GetSpriteForGhost(Ghost ghost)
    {
        string spr = "Sprites/Minimap/mini_"+ ghost.GetName();

        if(ghost.GetRecoveryAdjustedState() == GSTATE.frightened)
        {
            spr = "Sprites/Minimap/mini_frightened";
        }
        else if(ghost.GetRecoveryAdjustedState() == GSTATE.eaten)
        {
            spr = "Sprites/Minimap/mini_eyes";
        }

        return spr;
    }


    private void SetupImageGrid()
    {
        minimap_images = new List<List<Image>>();

        for(int x = 0; x < GameConfig.MINIMAP_WIDTH; x++)
        {
            minimap_images.Add(new List<Image>());
            for(int y = 0; y < GameConfig.MINIMAP_HEIGHT; y++)
            {
                string child_name = "mm"+ x.ToString() +"_"+ y.ToString();
                GameObject child_obj = GetChild(child_name);

                if(child_obj == null)
                {
                    Debug.LogError("Minimap failed to get child: "+ child_name);
                }
                else
                {
                    minimap_images[x].Add(child_obj.GetComponent<Image>());
                }
            }
        }
    }
}
