using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static PauseMenu instance;
    private bool test_mode = false;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void ContinueClicked()
    {
        LevelManager.Unpause();
    }


    public void ExitClicked()
    {
        Time.timeScale = 1;
        if(test_mode)
        {
            GameManager.ReturnFromTestMode();
        }
        else
        {
            GameManager.ReturnToMainMenu();
        }
    }


    public static void Deactivate()
    {
        instance.gameObject.SetActive(false);
    }


    public static void Activate(bool in_test_mode)
    {
        instance.gameObject.SetActive(true);
        instance.test_mode = in_test_mode;
    }
}
