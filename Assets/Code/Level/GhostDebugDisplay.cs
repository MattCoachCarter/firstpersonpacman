using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostDebugDisplay : UIHelper
{
    public static GhostDebugDisplay instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        DoGhostDebugDisplay();
    }


    public static void TurnOn()
    {
        instance.gameObject.SetActive(true);
    }


    public static void TurnOff()
    {
        instance.gameObject.SetActive(false);
    }


    private void DoGhostDebugDisplay()
    {
        string blinky_txt = "levelmanager is null";
        string pinky_txt = "levelmanager is null";
        string inky_txt = "levelmanager is null";
        string clyde_txt = "levelmanager is null";

        if(LevelManager.instance != null)
        {
            blinky_txt = "ghost list is null";
            pinky_txt = "ghost list is null";
            inky_txt = "ghost list is null";
            clyde_txt = "ghost list is null";
            List<Ghost> ghosts = LevelManager.instance.GetGhosts();
            if(ghosts != null)
            {
                int ghost_count = ghosts.Count;
                blinky_txt = "wrong number of ghosts @ "+ ghost_count.ToString();
                pinky_txt = "wrong number of ghosts @ "+ ghost_count.ToString();
                inky_txt = "wrong number of ghosts @ "+ ghost_count.ToString();
                clyde_txt = "wrong number of ghosts @ "+ ghost_count.ToString();

                if(ghost_count == 4)
                {
                    Vector2 blinky_pos = ghosts[0].GetLevelXY();
                    Vector2 pinky_pos = ghosts[1].GetLevelXY();
                    Vector2 inky_pos = ghosts[2].GetLevelXY();
                    Vector2 clyde_pos = ghosts[3].GetLevelXY();

                    blinky_txt = ((int)blinky_pos.x).ToString().PadLeft(2, '0') +" @ "+ ((int)blinky_pos.y).ToString().PadLeft(2, '0') +" @ "+ ghosts[0].GetState();
                    pinky_txt = ((int)pinky_pos.x).ToString().PadLeft(2, '0') +" @ "+ ((int)pinky_pos.y).ToString().PadLeft(2, '0') +" @ "+ ghosts[1].GetState();
                    inky_txt = ((int)inky_pos.x).ToString().PadLeft(2, '0') +" @ "+ ((int)inky_pos.y).ToString().PadLeft(2, '0') +" @ "+ ghosts[2].GetState();
                    clyde_txt = ((int)clyde_pos.x).ToString().PadLeft(2, '0') +" @ "+ ((int)clyde_pos.y).ToString().PadLeft(2, '0') +" @ "+ ghosts[3].GetState();
                }
            }

            SetChildText("blinky", blinky_txt);
            SetChildText("pinky", pinky_txt);
            SetChildText("inky", inky_txt);
            SetChildText("clyde", clyde_txt);
        }
    }
}
