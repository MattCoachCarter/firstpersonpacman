using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterboxHandler : UIHelper
{
    public static LetterboxHandler instance = null;
    private string target = "unletterbox";
    private string state = "idle";
    private const float ADJUST_AMOUNT = 80f;
    private const float START_POS = 460;
    private const float MOVE_AMOUNT = 800f;


    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(state == "moving")
        {
            MoveChildren();
        }
    }


    private void MoveChildren()
    {
        bool done = MoveTop();
        done = MoveBottom() || done;

        if(done)
        {
            state = "idle";
        }
    }


    private float GetUpdateMove()
    {
        float modifier = 1f;
        if(target == "unletterbox")
        {
            modifier = -1f;
        }

        return modifier * Time.deltaTime * MOVE_AMOUNT;
    }


    private float GetTopTarget()
    {
        if(target == "letterbox")
        {
            return START_POS - ADJUST_AMOUNT;
        }
        return START_POS;
    }


    private float GetBottomTarget()
    {
        if(target == "letterbox")
        {
            return (-1f * START_POS) + ADJUST_AMOUNT;
        }
        return (-1f * START_POS);
    }


    private bool MoveTop()
    {
        float top_target = GetTopTarget();
        Vector3 current_pos = GetChildPosition("Top");
        float new_y = current_pos.y - GetUpdateMove();

        bool done = false;
        if(target == "letterbox")
        {
            if(new_y <= top_target)
            {
                new_y = top_target;
                done = true;
            }
        }
        else
        {
            if(new_y >= top_target)
            {
                new_y = top_target;
                done = true;
            }
        }

        Vector3 new_pos = new Vector3(current_pos.x, new_y, current_pos.z);
        SetChildPosition("Top", new_pos);
        return done;
    }


    private bool MoveBottom()
    {
        float bottom_target = GetBottomTarget();
        Vector3 current_pos = GetChildPosition("Bottom");
        float new_y = current_pos.y + GetUpdateMove();

        bool done = false;
        if(target == "letterbox")
        {
            if(new_y >= bottom_target)
            {
                new_y = bottom_target;
                done = true;
            }
        }
        else
        {
            if(new_y <= bottom_target)
            {
                new_y = bottom_target;
                done = true;
            }
        }

        Vector3 new_pos = new Vector3(current_pos.x, new_y, current_pos.z);
        SetChildPosition("Bottom", new_pos);
        return done;
    }


    public static void Letterbox()
    {
        if(instance != null)
        {
            instance.DoLetterbox();
        }
    }


    public static void Unletterbox()
    {
        if(instance != null)
        {
            instance.DoUnletterbox();
        }
    }


    private void DoLetterbox()
    {
        target = "letterbox";
        state = "moving";
    }


    private void DoUnletterbox()
    {
        target = "unletterbox";
        state = "moving";
    }
}
