using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelletStatus : UIHelper
{
    public static PelletStatus instance = null;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Reset()
    {
        SetChildText("PelletPercentage", "0");
    }


    public void PctUpdate(int pellets_collected, int pellets_required)
    {
        int pct = (int)Mathf.Floor((((float)pellets_collected) / ((float)pellets_required)) * 100f);
        SetChildText("PelletPercentage", pct.ToString());
    }
}
