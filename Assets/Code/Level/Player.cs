using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Mover
{
    public static Player instance = null;

    private GameObject camera_handle;
    private string state = "waiting";
    private float rotation_target = 0f;
    private float rotation_speed = 0f;
    private bool camera_flipped = false;
    private float time_since_lost = 0f;
    private bool debug_enabled = false;
    private bool running = false;
    private float explode_delay = 0f;


    void Awake()
    {
        if(instance != null)
        {
            Common.SafeDestroy(instance.gameObject);
        }
        instance = this;
    }


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        if(running)
        {
            base.Update();

            HandleInput();
            HandleRotation();
            HandleLosing();
            HandleCameraRotation();
        }
    }


    protected void FixedUpdate()
    {
        if(running)
        {
            HandleMovement();
        }
    }


    public static void Run()
    {
        TakeControlOfCamera();
        instance.state = "moving";
        instance.running = true;
    }


    public static void Unrun()
    {
        instance.running = false;
    }


    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        string collision_tag = (string)other.gameObject.tag;

        if(collision_tag == "pellet")
        {
            CollectPellet(other.gameObject);
        }
        else if(collision_tag == "powerpellet")
        {
            CollectPowerPellet(other.gameObject);
        }
    }


    private void HandleInput()
    {
        if(state == "moving")
        {
            if(InputManager.WasJustPressed(BTNINPUT.left))
            {
                state = "rotate_start_counter_clockwise";
            }
            else if(InputManager.WasJustPressed(BTNINPUT.right))
            {
                state = "rotate_start_clockwise";
            }
            else if(InputManager.WasJustPressed(BTNINPUT.down))
            {
                state = "rotate_start_backwards";
            }
        }

        if(InputManager.WasJustPressed(BTNINPUT.camera_flip))
        {
            CameraFlip();
        }
        else if(InputManager.IsUp(BTNINPUT.camera_flip))
        {
            if(camera_flipped)
            {
                CameraUnflip();
            }
        }

        if(InputManager.WasJustPressed(BTNINPUT.toggle_debug))
        {
            ToggleDebug();
        }
    }


    private void ToggleDebug()
    {
        if(debug_enabled)
        {
            LevelManager.instance.EnableCeiling();
            GhostDebugDisplay.TurnOff();
        }
        else
        {
            LevelManager.instance.DisableCeiling();
            GhostDebugDisplay.TurnOn();
        }
        debug_enabled = !debug_enabled;
    }


    private void HandleMovement()
    {
        if(state == "moving")
        {
            if(InputManager.IsDown(BTNINPUT.stop))
            {
                HardStop();
            }
            else
            {
                DoForwardMovement();
            }
        }
    }


    private void DoForwardMovement()
    {
        ForwardMovementAtSpeed(GameConfig.PLAYER_MOVE_SPEED, GameConfig.PLAYER_TOP_SPEED);
    }


    private void HandleRotation()
    {
        if(state == "rotate_start_counter_clockwise" ||
           state == "rotate_start_clockwise" ||
           state == "rotate_start_backwards")
        {
            StartRotation();
        }
        else if(state == "rotating")
        {
            DoRotation();
        }
    }


    private void StartRotation()
    {
        rotation_speed = GameConfig.ROTATION_SPEED;

        HardStop();

        // Get the current rotation:
        float current_rotation = GetCurrentRotation();
        
        // Determine target rotation:
        if(state == "rotate_start_counter_clockwise")
        {
            rotation_target = current_rotation - 90f;
            rotation_speed *= -1;
        }
        else if(state == "rotate_start_clockwise")
        {
            rotation_target = current_rotation + 90f;
        }
        else // backwards
        {
            rotation_target = current_rotation + 180f;
            rotation_speed *= 2;
        }

        // Normalize for euler angles
        if(rotation_target > 180f)
        {
            rotation_target -= 360f;
        }

        state = "rotating";
    }


    private void DoRotation()
    {
        float current_rotation = GetCurrentRotation();
        float next_rotation = current_rotation + rotation_speed;

        if(Mathf.Abs((next_rotation + 360) - (rotation_target + 360)) < Mathf.Abs(rotation_speed))
        {
            transform.eulerAngles = new Vector3(
                transform.eulerAngles.x,
                rotation_target,
                transform.eulerAngles.z
            );
            state = "moving";
        }
        else
        {
            transform.Rotate(0f, rotation_speed, 0f, Space.Self);
        }
    }


    private float GetCurrentRotation()
    {
        float r = transform.eulerAngles.y;
        if(r > 180f)
        {
            r -= 360f;
        }

        return r;
    }


    public string GetCurrentDirection()
    {
        float current_rotation = GetCurrentRotation();
        if(state == "rotating")
        {
            current_rotation = rotation_target;
        }

        string current_direction = "up";
        if(current_rotation == 90)
        {
            current_direction = "right";
        }
        else if(current_rotation == 180 || current_rotation == -180)
        {
            current_direction = "down";
        }
        else if(current_rotation == -90)
        {
            current_direction = "left";
        }
        else if(current_rotation != 0)
        {
            Debug.LogError("Expected rotation value when determing player facing direction: "+ current_rotation);
        }

        return current_direction;
    }


    public static void TakeControlOfCamera()
    {
        instance.DoTakeControlOfCamera();
    }


    private void DoTakeControlOfCamera()
    {
        camera_handle = GameObject.Find("Main Camera");
        camera_handle.transform.SetParent(this.transform);
        camera_handle.transform.position = transform.position + GameConfig.CAMERA_PLAYER_OFFSET;
        camera_handle.transform.eulerAngles = transform.eulerAngles;
    }


    private void CollectPellet(GameObject pellet_game_object)
    {
        Common.SafeDestroy(pellet_game_object);
        LevelManager.instance.PelletConsumed();
    }


    private void CollectPowerPellet(GameObject power_pellet_game_object)
    {
        Common.SafeDestroy(power_pellet_game_object);
        LevelManager.instance.PowerPelletConsumed();
    }


    public void DoLose(GameObject ghost_object_responsible)
    {
        state = "lost";
        explode_delay = GameConfig.EXPLODE_DELAY;
        CameraUnflip();
        HandleCameraRotation();
        LetterboxHandler.Letterbox();
        NotificationHandler.instance.Notify("you lose");
        HardStop();
        DoLoseCamera();
    }


    private void DoLoseCamera()
    {
        camera_handle.transform.parent = null;
        camera_handle.transform.SetParent(null);
        camera_handle.transform.position = new Vector3(camera_handle.transform.position.x,
                                                       camera_handle.transform.position.y + 10f,
                                                       camera_handle.transform.position.z);
    }


    private void HandleLosing()
    {
        if(state == "lost")
        {
            if(explode_delay > 0f)
            {
                explode_delay -= Time.deltaTime;
                if(explode_delay <= 0f)
                {
                    Explode();
                }
            }

            time_since_lost += Time.deltaTime;
            float time_since_lost_squared = time_since_lost * time_since_lost;
            Vector3 camera_move_vector = transform.up * Time.deltaTime * (10f * time_since_lost_squared);
            camera_handle.transform.position = camera_handle.transform.position + camera_move_vector;

            GameObject floor = LevelManager.instance.GetFloor();
            float z_x_approach = Time.deltaTime * time_since_lost_squared * 4f;
            float new_z = Common.ApproachTarget(camera_handle.transform.position.z,
                                                z_x_approach,
                                                floor.transform.position.z);
            float new_x = Common.ApproachTarget(camera_handle.transform.position.x,
                                                z_x_approach,
                                                floor.transform.position.x);
            camera_handle.transform.position = new Vector3(new_x,
                                                           camera_handle.transform.position.y,
                                                           new_z);
            HandleCameraRotation();
        }
    }


    private void LookAtSelf()
    {
        camera_handle.transform.LookAt(transform);
    }


    private void HandleCameraRotation()
    {
        float x_val = transform.eulerAngles.x;
        float y_val = transform.eulerAngles.y;
        float z_val = transform.eulerAngles.z;

        if(state == "lost")
        {
            x_val = 90f;
            z_val = 0f;
            y_val = 0f;
        }
        else if(camera_flipped)
        {
            y_val += 180f;
        }

        camera_handle.transform.eulerAngles = new Vector3(x_val, y_val, z_val);
    }

    
    private void CameraFlip()
    {
        camera_flipped = true;
        LetterboxHandler.Letterbox();
    }
    

    private void CameraUnflip()
    {
        camera_flipped = false;
        LetterboxHandler.Unletterbox();
    }


    private void Explode()
    {
        transform.Find("Sphere").gameObject.SetActive(false);
        transform.Find("DeathObjects").gameObject.SetActive(true);
    }
}
