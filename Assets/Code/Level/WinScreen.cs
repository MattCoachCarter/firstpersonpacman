using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScreen : UIHelper
{
    public static WinScreen instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void ExitClicked()
    {
        Time.timeScale = 1;
        GameManager.jump_to_level_select = true;
        GameManager.ReturnToMainMenu();
    }


    private string FormatTime(float level_time)
    {
        int minutes = (int)Mathf.Floor(level_time / 60f);
        int seconds = ((int)Mathf.Floor(level_time)) - (minutes * 60);

        string s = minutes.ToString();
        if(minutes < 10)
        {
            s = "0"+ minutes.ToString();
        }

        s += ":";
        if(seconds < 10)
        {
            s += "0";
        }
        
        s += seconds.ToString();
        return s;
    }


    public static void Deactivate()
    {
        instance.gameObject.SetActive(false);
    }


    public static void Activate(float level_time, int deaths)
    {
        instance.gameObject.SetActive(true);
        instance.SetChildText("Time", instance.FormatTime(level_time));
        instance.SetChildText("Deaths", deaths.ToString());
    }
}
