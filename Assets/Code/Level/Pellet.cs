using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pellet : Floaty
{
    // Start is called before the first frame update
    void Start()
    {
        DetermineFloatAnimationDelay();
    }


    // Don't need to override Update


    private void DetermineFloatAnimationDelay()
    {
        GameObject player = Player.instance.gameObject;
        float_animation_delay = Vector3.Distance(player.transform.position, transform.position) / 25f;
    }
}