using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostIndicator : UIHelper
{
    public static GhostIndicator instance = null;
    private List<Ghost> ghosts = null;
    private bool ready = false;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Setup();
    }


    // Update is called once per frame
    void Update()
    {
        if(!ready)
        {
            Setup();
        }
    }


    private void Setup()
    {
        if(LevelManager.instance != null)
        {
            ghosts = LevelManager.instance.GetGhosts();
            if(ghosts != null && ghosts.Count == 4)
            {
                ready = true;
                GhostUpdate();
            }
        }
    }


    public void Reset()
    {
        ready = false;
        Setup();
    }


    public void GhostUpdate()
    {
        if(ready)
        {
            foreach(Ghost ghost in ghosts)
            {
                SingleGhostUpdate(ghost);
            }
        }
    }


    private void SingleGhostUpdate(Ghost ghost)
    {
        GSTATE ghost_state = ghost.GetRecoveryAdjustedState();
        string ghost_name = ghost.GetName();

        string sprite = DetermineSprite(ghost_name, ghost_state);
        string child_name = DetermineChildName(ghost_name);
        float img_alpha = DetermineSpriteTransparency(ghost_state);

        SetChildImage(child_name, sprite, img_alpha);
    }


    private int GetFrightenTimePercentageLeft()
    {
        float current_timer = LevelManager.instance.GetFrightenedTimer();
        float timer_start = LevelManager.instance.GetFrightenTime();

        return (int)Mathf.Round((current_timer / timer_start) * 100f);
    }


    private string DetermineSprite(string ghost_name, GSTATE ghost_state)
    {
        if(ghost_state == GSTATE.frightened)
        {
            string spr = "Sprites/blue_ghost";
            if(LevelManager.instance.ShouldFrightenFlashWhite())
            {
                spr = "Sprites/white_ghost";
            }

            return spr;
        }
        else if(ghost_state == GSTATE.eaten)
        {
            return "Sprites/ghost_eyes";
        }

        return "Sprites/"+ ghost_name;
    }


    private string DetermineChildName(string ghost_name)
    {
        return Common.CapitalizeStr(ghost_name) +"Indicator";
    }


    private float DetermineSpriteTransparency(GSTATE ghost_state)
    {
        if(ghost_state == GSTATE.none ||
           ghost_state == GSTATE.waiting)
        {
            return 0.125f;
        }

        return 1f;
    }
}
