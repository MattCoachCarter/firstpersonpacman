using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : Floaty
{
    protected bool is_floaty = false;
    protected Rigidbody rbody;
    protected float portal_cooldown = 0f;


    // Start is called before the first frame update
    protected virtual void Start()
    {
        rbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    protected override void Update()
    {
        if(is_floaty)
        {
            base.Update();
        }
        HandlePortalCooldown();
    }


    protected void HandlePortalCooldown()
    {
        if(portal_cooldown > 0f)
        {
            portal_cooldown -= Time.deltaTime;
        }
    }


    protected virtual void OnTriggerEnter(Collider other)
    {
        string collision_tag = (string)other.gameObject.tag;

        if(collision_tag == "portal")
        {
            GoThroughPortal(other.gameObject);
        }
    }


    protected void HardStop()
    {
        // Stop movement:
        rbody.velocity = rbody.velocity.normalized * 0f;
    }


    private void GoThroughPortal(GameObject portal_game_object)
    {
        if(portal_cooldown <= 0f)
        {
            Debug.Log("Going through portal");
            portal_cooldown = 2f;

            Portal portal = portal_game_object.GetComponent<Portal>();
            GameObject other_portal_game_object = portal.link;

            transform.position = new Vector3(other_portal_game_object.transform.position.x,
                                             transform.position.y,
                                             other_portal_game_object.transform.position.z) + transform.forward;

            WentThroughPortal();
        }
    }


    protected void ForwardMovementAtSpeed(float move_speed, float max_speed)
    {
        Vector3 horiz_move = new Vector3(0, 0, 0);  // This would be for strafing which we do not want
        Vector3 vert_move = new Vector3(0, 0, 0);

        vert_move += transform.forward;

        if(!Common.IsRoughlyZero(vert_move.magnitude) || !Common.IsRoughlyZero(horiz_move.magnitude))
        {
            Vector3 movement = Vector3.Normalize(vert_move + horiz_move);
            rbody.AddForce(movement * move_speed);
        }

        if(rbody.velocity.magnitude > max_speed)
        {
            rbody.velocity = rbody.velocity.normalized * max_speed;
        }
    }


    protected virtual void WentThroughPortal()
    {
        
    }
}
