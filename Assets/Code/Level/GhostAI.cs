using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostAI
{
    private Ghost parent = null;
    private string name = null;
    private LevelData level_data = null;
    private string last_move_direction = "";
    private int current_scatter_target = 1;
    private Vector2 current_x_y = new Vector2(0, 0);


    public GhostAI(Ghost p, string n, LevelData ldata)
    {
        parent = p;
        name = n;
        level_data = ldata;
    }


    public string GetLastMoveDirection()
    {
        return last_move_direction;
    }


    public Vector3 GetFlipAroundMoveTarget()
    {
        current_x_y = GetMyCurrentXY();
        // Debug.Log("I'm at: "+ current_x_y);

        string move_direction = "down";
        if(last_move_direction == "left")
        {
            move_direction = "right";
        }
        else if(last_move_direction == "right")
        {
            move_direction = "left";
        }
        else if(last_move_direction == "down")
        {
            move_direction = "up";
        }

        List<string> possible_move_directions = GetPossibleMoveDirections(true);
        if(!possible_move_directions.Contains(move_direction))
        {
            move_direction = null;
            foreach(string possible_move_dir in possible_move_directions)
            {
                move_direction = possible_move_dir;
                if(move_direction != last_move_direction)
                {
                    break;
                }
            }

            if(move_direction == null)
            {
                Debug.LogError("("+ name +") could not find flip around direction, prev was: "+ last_move_direction);
                Debug.Break();
                move_direction = last_move_direction;
            }
        }
        
        return MoveInDirection(move_direction);
    }


    public Vector3 GetNextMoveTarget()
    {
        current_x_y = GetMyCurrentXY();
        // Debug.Log("I'm at: "+ current_x_y);

        GSTATE state = parent.GetState();

        if(state == GSTATE.frightened)
        {
            return GetFrightenedMoveTarget();
        }
        else if(state == GSTATE.release)
        {
            return GetReleaseMoveTarget();
        }
        else if(state == GSTATE.eaten)
        {
            return GetEatenMoveTarget();
        }
        else if(state != GSTATE.scatter && state != GSTATE.chase)
        {
            Debug.LogError(name +" asked for move target for unexpected state: "+ state);
        }

        if(name == "blinky")
        {
            return TargetForBlinky(state);
        }
        else if(name == "pinky")
        {
            return TargetForPinky(state);
        }
        else if(name == "inky")
        {
            return TargetForInky(state);
        }
        
        return TargetForClyde(state);
    }


    private Vector2 GetMyCurrentXY()
    {
        return parent.GetLevelXY();
    }


    private Vector2 GetPlayerCurrentXY()
    {
        return Common.ThreeDToLevelCoords(Player.instance.gameObject.transform.position);
    }


    private void SwapScatterTarget()
    {
        if(current_scatter_target == 1)
        {
            current_scatter_target = 2;
        }
        else
        {
            current_scatter_target = 1;
        }
    }


    private Vector2 DirectionToRelativeLevelCoords(string direction)
    {
        int x_adjust = 0;
        int y_adjust = 0;

        if(direction == "up")
        {
            y_adjust = 1;
        }
        else if(direction == "down")
        {
            y_adjust = -1;
        }
        else if(direction == "left")
        {
            x_adjust = -1;
        }
        else
        {
            x_adjust = 1;
        }

        return new Vector2(current_x_y.x + x_adjust, current_x_y.y + y_adjust);
    }


    private bool CanMoveToSpace(int space_being_occupied_by)
    {
        bool can_move_through_gate = (parent.GetState() == GSTATE.release || parent.GetState() == GSTATE.eaten);

        return space_being_occupied_by != LevelData.WALL && 
               space_being_occupied_by != LevelData.TALL_WALL && 
               (space_being_occupied_by != LevelData.GATE || can_move_through_gate);
    }


    private Vector3 GetCoordsOfSpaceInDirection(string direction)
    {
        Vector2 target_space = new Vector2(0, 0);

        if(direction == "up")
        {
            target_space = new Vector2(current_x_y.x, current_x_y.y + 1);
        }
        else if(direction == "down")
        {
            target_space = new Vector2(current_x_y.x, current_x_y.y - 1);
        }
        else if(direction == "left")
        {
            target_space = new Vector2(current_x_y.x - 1, current_x_y.y);
        }
        else // right
        {
            target_space = new Vector2(current_x_y.x + 1, current_x_y.y);
        }

        return level_data.GetBottomLeftCoordOfLevelXY((int)target_space.x,
                                                      (int)target_space.y,
                                                      1f);
    }


    private string GetBestMoveDirForTarget(int target_x, int target_y)
    {
        return GetBestMoveDirForTarget(new Vector2(target_x, target_y));
    }


    private string GetBestMoveDirForTarget(Vector2 target)
    {
        List<string> possible_move_directions = GetPossibleMoveDirections();

        string best_direction = null;
        float shortest_dist = 0f;
        foreach(string move_dir in possible_move_directions)
        {
            Vector2 move_coords = DirectionToRelativeLevelCoords(move_dir);
            float dist = Vector2.Distance(target, move_coords);
            if(best_direction == null || shortest_dist > dist)
            {
                shortest_dist = dist;
                best_direction = move_dir;
            }
            else if(shortest_dist == dist)
            {
                if(last_move_direction == move_dir)
                {
                    best_direction = move_dir;
                }
                else if((best_direction == "left" || best_direction == "right") &&
                   (move_dir == "up" || move_dir == "down"))
                {
                    best_direction = move_dir;
                }
            }
        }

        return best_direction;
    }

    private List<string> GetPossibleMoveDirections() { return GetPossibleMoveDirections(false); }
    private List<string> GetPossibleMoveDirections(bool ignore_previous_mode)
    {
        List<string> possible_move_directions = new List<string>();
        
        int above = level_data.GetData(((int)current_x_y.x), ((int)current_x_y.y) + 1);
        int below = level_data.GetData(((int)current_x_y.x), ((int)current_x_y.y) - 1);
        int left  = level_data.GetData(((int)current_x_y.x) - 1, ((int)current_x_y.y));
        int right = level_data.GetData(((int)current_x_y.x) + 1, ((int)current_x_y.y));

        // Debug.Log("Above: "+ above);
        // Debug.Log("Below: "+ below);
        // Debug.Log(" Left: "+ left);
        // Debug.Log("Right: "+ right);

        if((last_move_direction != "down" || ignore_previous_mode) && CanMoveToSpace(above))
        {
            possible_move_directions.Add("up");
        }
        if((last_move_direction != "right" || ignore_previous_mode) && CanMoveToSpace(left))
        {
            possible_move_directions.Add("left");
        }
        if((last_move_direction != "left" || ignore_previous_mode) && CanMoveToSpace(right))
        {
            possible_move_directions.Add("right");
        }
        if((last_move_direction != "up" || ignore_previous_mode) && CanMoveToSpace(below))
        {
            possible_move_directions.Add("down");
        }

        if(possible_move_directions.Count < 1)
        {
            if(ignore_previous_mode)
            {
                Debug.LogError("Failed to find a move direction even in ignore-previous mode ("+ name +").");
                Debug.LogWarning("Falling back to just moving up");
                possible_move_directions.Add("up");
                Debug.Break();
            }
            else
            {
                Debug.LogError("Tried to get possible move directions but found none ("+ name +") (prev: "+ last_move_direction +")!");
                return GetPossibleMoveDirections(true);
            }
        }

        return possible_move_directions;
    }


    private Vector3 GetFrightenedMoveTarget()
    {
        List<string> possible_move_directions = GetPossibleMoveDirections();
        int move_direction_index = Random.Range(0, possible_move_directions.Count);
        string move_direction = possible_move_directions[move_direction_index];

        return MoveInDirection(move_direction);
    }


    private Vector3 MoveInDirection(string move_direction)
    {
        last_move_direction = move_direction;
        return GetCoordsOfSpaceInDirection(move_direction);
    }


    private Vector3 GetReleaseMoveTarget()
    {
        int target_x = level_data.ghost_house_exit_x;
        int target_y = level_data.ghost_house_exit_y;
        return MoveInDirection(GetBestMoveDirForTarget(target_x, target_y));
    }


    private Vector3 GetEatenMoveTarget()
    {
        int target_x = level_data.eaten_recovery_x;
        int target_y = level_data.eaten_recovery_y;
        return MoveInDirection(GetBestMoveDirForTarget(target_x, target_y));
    }


    private bool MadeItToLocation(int location_x, int location_y)
    {
        current_x_y = GetMyCurrentXY();
        return current_x_y.x == location_x && current_x_y.y == location_y;
    }


    public bool IsFullyReleased()
    {
        int target_x = level_data.ghost_house_exit_x;
        int target_y = level_data.ghost_house_exit_y;
        return MadeItToLocation(target_x, target_y);
    }


    public bool IsFullyEaten()
    {
        int target_x = level_data.eaten_recovery_x;
        int target_y = level_data.eaten_recovery_y;
        return MadeItToLocation(target_x, target_y);
    }


    private Vector3 TargetForBlinky(GSTATE state)
    {
        if(state == GSTATE.scatter)
        {
            return BlinkyScatter();
        }
        return BlinkyChase();
    }


    private Vector3 BlinkyScatter()
    {
        int target_x = level_data.blinky_scatter_x;
        int target_y = level_data.blinky_scatter_y;

        return MoveInDirection(GetBestMoveDirForTarget(target_x, target_y));
    }


    private Vector3 BlinkyChase()
    {
        return MoveInDirection(GetBestMoveDirForTarget(GetPlayerCurrentXY()));
    }


    private Vector3 TargetForPinky(GSTATE state)
    {
        if(state == GSTATE.scatter)
        {
            return PinkyScatter();
        }
        return PinkyChase();
    }


    private Vector3 PinkyScatter()
    {
        int target_x = level_data.pinky_scatter_x;
        int target_y = level_data.pinky_scatter_y;
        
        return MoveInDirection(GetBestMoveDirForTarget(target_x, target_y));
    }


    private Vector2 GetTargetInFrontOfPlayer(int amount)
    {
        Vector2 player_x_y = GetPlayerCurrentXY();
        string player_direction = Player.instance.GetCurrentDirection();

        int adjust_x = 0;
        int adjust_y = 0;
        if(player_direction == "up")
        {
            adjust_y = amount;
            adjust_x = (-1 * amount);
        }
        else if(player_direction == "down")
        {
            adjust_y = (-1 * amount);
        }
        else if(player_direction == "left")
        {
            adjust_x = (-1 * amount);
        }
        else if(player_direction == "right")
        {
            adjust_x = amount;
        }

        return new Vector2(player_x_y.x + adjust_x,
                           player_x_y.y + adjust_y);
    }


    private Vector3 PinkyChase()
    {
        return MoveInDirection(GetBestMoveDirForTarget(GetTargetInFrontOfPlayer(4)));
    }


    private Vector3 TargetForInky(GSTATE state)
    {
        if(state == GSTATE.scatter)
        {
            return InkyScatter();
        }
        return InkyChase();
    }


    private Vector3 InkyScatter()
    {
        int target_x = level_data.inky_scatter_x;
        int target_y = level_data.inky_scatter_y;
        
        return MoveInDirection(GetBestMoveDirForTarget(target_x, target_y));
    }


    private Vector3 InkyChase()
    {
        Vector2 my_pos = GetMyCurrentXY();
        Vector2 player_front_pos = GetTargetInFrontOfPlayer(2);
        Vector2 blinky_pos = LevelManager.instance.GetBlinky().GetLevelXY();
        int x_diff = (int)Mathf.Abs(((int)player_front_pos.x) - blinky_pos.x);
        int y_diff = (int)Mathf.Abs(((int)player_front_pos.y) - blinky_pos.y);
        int x_adjust = x_diff;
        int y_adjust = y_diff;
    
        if(blinky_pos.x > player_front_pos.x)
        {
            x_adjust *= -1;
        }
        if(blinky_pos.y > player_front_pos.y)
        {
            y_adjust *= -1;
        }

        Vector2 target = new Vector2(player_front_pos.x + x_adjust,
                                     player_front_pos.y + y_adjust);
        return MoveInDirection(GetBestMoveDirForTarget(target));
    }


    private Vector3 TargetForClyde(GSTATE state)
    {
        if(state == GSTATE.scatter)
        {
            return ClydeScatter();
        }
        return ClydeChase();
    }


    private Vector3 ClydeChase()
    {
        float distance_to_player = Vector2.Distance(GetPlayerCurrentXY(), GetMyCurrentXY());

        if(distance_to_player < 8f)
        {
            return ClydeScatter();
        }
        return BlinkyChase();
    }


    private Vector3 ClydeScatter()
    {
        int target_x = level_data.clyde_scatter_x;
        int target_y = level_data.clyde_scatter_y;

        return MoveInDirection(GetBestMoveDirForTarget(target_x, target_y));
    }
}
