using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floaty : MonoBehaviour
{
    protected float float_animation_delay = 0f;
    protected float float_wait_time = 0f;
    protected float current_float_offset = 0f;
    protected float current_float_direction = 1f;
    protected int float_direction_switch_cooldown = 0;
    protected float float_speed = 2.2f;
    protected float max_float = 0.75f;
    protected float float_easing_adjust = 1f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        DoFloatAnimation();
    }


    private void DoFloatAnimation()
    {
        if(float_wait_time >= float_animation_delay)
        {
            AnimateUpAndDown();
        }
        else
        {
            float_wait_time += Time.deltaTime;
        }
    }


    private void AnimateUpAndDown()
    {
        float abs_current_float_offset = Mathf.Abs(current_float_offset);
        if(abs_current_float_offset >= max_float && float_direction_switch_cooldown <= 0)
        {
            current_float_direction *= -1;
            float_direction_switch_cooldown = 30; // minimum 30 frames until we can switch directions again
        }

        float_direction_switch_cooldown--;

        float distance_to_target = (abs_current_float_offset / max_float);
        float easing_amount = -((Mathf.Cos(Mathf.PI * distance_to_target) - 1f) / float_easing_adjust) + 1f;

        float movement_amount = ((float_speed * current_float_direction) / easing_amount) * Time.deltaTime;
        current_float_offset += movement_amount;
        transform.position = new Vector3(transform.position.x,
                                         transform.position.y + movement_amount,
                                         transform.position.z);
    }
}
