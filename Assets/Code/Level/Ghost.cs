using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GSTATE { none, pause, ready, release, frightened, scatter, chase, waiting, eaten, recover };

public class Ghost : Mover
{
    private string ghost_name = null;
    private GSTATE state = GSTATE.none;
    private GhostAI ai = null;
    private LevelData level_data = null;
    private Vector3 move_target = new Vector3(0, 0, 0);
    private float target_move_tollerance = 0.75f;
    private float previous_target_distance = 99999f;
    private GSTATE state_to_recover_to = GSTATE.none;
    private float recovery_grace_time = 0f;
    private Color ghost_color = new Color(1f, 1f, 1f, 1f);
    private Color frighten_color_blue = new Color(0f, 0f, 1f, 1f);
    private Color frighten_color_white = new Color(1f, 1f, 1f, 1f);
    private bool get_brighter = false;
    private bool get_dimmer = false;
    private bool running = false;

    private Material default_material = null;
    private Material frightened_material_blue = null;
    private Material frightened_material_white = null;
    private Material eyes_material = null;

    private const float RECOVERY_GRACE_TIME_RESET_SEC = 1.25f;
    private const float BRIGHTNESS_CHANGE_PER_TICK = 2f;



    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();  // Call Mover Start
        is_floaty = true;
        float_speed = 2.5f;
        max_float = 0.5f;
        // rbody.detectCollisions = false;
    }

    // Update is called once per frame
    protected override void Update()
    {
        if(running)
        {
            base.Update();  // Call Mover/Floaty update
            HandleStateNonMovement();
            HandleFrightenedStyling();
            OutOfBoundsCheck();
        }
    }


    protected void FixedUpdate()
    {
        if(running)
        {
            HandleMovement();
        }
    }


    public void Run()
    {
        running = true;
    }


    public void Unrun()
    {
        running = false;
    }


    private void HandleFrightenedStyling()
    {
        if(state == GSTATE.frightened)
        {
            if(LevelManager.instance.ShouldFrightenFlashWhite())
            {
                FrightenWhiteMaterial();
                LightColorFrightenedWhite();
            }
            else
            {
                FrightenBlueMaterial();
                LightColorFrightenedBlue();
            }
        }
    }


    private void OutOfBoundsCheck()
    {
        Vector2 current_x_y = GetLevelXY();
        if(level_data.IsOutOfBounds(current_x_y))
        {
            Debug.LogError(ghost_name +" is out of bounds, attempting to recover by returning to starting position");
            HardStop();

            int level_reset_x = level_data.blinky_start_x;
            int level_reset_y = level_data.blinky_start_y;
            if(ghost_name == "pinky")
            {
                level_reset_x = level_data.pinky_start_x;
                level_reset_y = level_data.pinky_start_y;
            }
            else if(ghost_name == "inky")
            {
                level_reset_x = level_data.inky_start_x;
                level_reset_y = level_data.inky_start_y;
            }
            else if(ghost_name == "clyde")
            {
                level_reset_x = level_data.clyde_start_x;
                level_reset_y = level_data.clyde_start_y;
            }

            transform.position = level_data.GetBottomLeftCoordOfLevelXY(level_reset_x,
                                                                        level_reset_y,
                                                                        transform.position.y);

            if(ghost_name != "blinky")
            {
                ChangeState(GSTATE.release);
            }
        }
    }


    private void ReenablePlayerCollisions()
    {
        Collider player_collider = Player.instance.gameObject.GetComponent<Collider>();
        Collider ghost_collider = gameObject.GetComponent<Collider>();
        Physics.IgnoreCollision(player_collider, ghost_collider, false);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "player")
        {
            Debug.Log("("+ ghost_name +") Ghost collided with player");

            // Turn off physics collisions with the player until the ghost is
            // reset or something
            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(),
                                    gameObject.GetComponent<Collider>(),
                                    true);

            if(GetRecoveryAdjustedState() == GSTATE.frightened)
            {
                GetEaten();
            }
            else if(GetRecoveryAdjustedState() != GSTATE.eaten)
            {
                
                foreach(Ghost ghost in LevelManager.instance.GetGhosts())
                {
                    ghost.get_dimmer = true;
                }
                get_dimmer = false;
                get_brighter = true;
                GetLightGameObject().GetComponent<Light>().shadows = LightShadows.None;
                LevelManager.instance.DoLevelLose(this.gameObject);
            }
        }
    }

    
    private void GetEaten()
    {
        ChangeState(GSTATE.eaten);
        ShowEatenNotification();
    }


    public void ChangeState(GSTATE new_state)
    {
        GSTATE prev_state = GetState();
        SetState(new_state);

        if(prev_state != new_state)
        {
            if(new_state == GSTATE.frightened)
            {
                LightColorFrightenedBlue();
                FrightenBlueMaterial();
            }
            else if(prev_state == GSTATE.frightened)
            {
                ResetLightColor();
                ResetMaterial();
            }

            if(new_state == GSTATE.eaten)
            {
                DisableLight();
                EyesMaterial();
            }
            else if(prev_state == GSTATE.eaten)
            {
                EnableLight();
                ResetMaterial();
            }

            if(state == GSTATE.release)
            {
                ShowReleaseNotification();
                DoNewTargetMovement();
            }
            else if(state != GSTATE.pause)
            {
                FlipAround();
            }
        }
    }


    private void SetState(GSTATE new_state)
    {
        state = new_state;
        if(GhostIndicator.instance != null)
        {
            GhostIndicator.instance.GhostUpdate();
        }
    }


    public void Setup(string n, LevelData ldata)
    {
        SetState(GSTATE.waiting);
        ghost_name = n;
        level_data = ldata;

        if(ghost_name == "blinky")
        {
            SetState(GSTATE.ready);
        }

        DetermineColor();
        LoadNeededMaterials();
        StyleBasedOnName();
        ai = new GhostAI(this, ghost_name, level_data);
    }


    private void LoadNeededMaterials()
    {
        default_material = Common.LoadMaterial("Materials/"+ ghost_name);
        frightened_material_blue = Common.LoadMaterial("Materials/frightened_blue");
        frightened_material_white = Common.LoadMaterial("Materials/frightened_white");
        eyes_material = Common.LoadMaterial("Materials/eyes");
    }


    public GSTATE GetRecoveryAdjustedState()
    {
        if(state == GSTATE.recover)
        {
            return state_to_recover_to;
        }
        return state;
    }


    public GSTATE GetState()
    {
        return state;
    }


    public string GetName()
    {
        return ghost_name;
    }


    private void DetermineColor()
    {
        if(ghost_name == "blinky")
        {
            ghost_color = new Color(1f, 0f, 0f, 1f);
        }
        else if(ghost_name == "pinky")
        {
            ghost_color =  new Color(1f, 0.72f, 1f, 1f);
        }
        else if(ghost_name == "inky")
        {
            ghost_color = new Color(0f, 1f, 1f, 1f);
        }
        else
        {
            ghost_color = new Color(1f, 0.79f, 0.32f, 1f);
        }
    }


    private void StyleBasedOnName()
    {
        ResetMaterial();
        ResetLightColor();
    }


    private void ResetMaterial()
    {
        SetMaterial(default_material);
    }


    private void FrightenBlueMaterial()
    {
        SetMaterial(frightened_material_blue);
    }


    private void FrightenWhiteMaterial()
    {
        SetMaterial(frightened_material_white);
    }


    private void EyesMaterial()
    {
        SetMaterial(eyes_material);
    }


    private void SetMaterial(Material m)
    {
        GameObject model = transform.Find("Model").gameObject;
        model.GetComponent<Renderer>().material = m;
    }


    private void LightColorFrightenedBlue()
    {
        SetLightColor(frighten_color_blue);
    }


    private void LightColorFrightenedWhite()
    {
        SetLightColor(frighten_color_white);
    }


    private void ResetLightColor()
    {
        SetLightColor(ghost_color);
    }


    private GameObject GetLightGameObject()
    {
        return transform.Find("Point Light").gameObject;
    }


    private void SetLightColor(Color c)
    {
        GetLightGameObject().GetComponent<Light>().color = c;
    }


    private void IncrementLightBrightness()
    {
        GetLightGameObject().GetComponent<Light>().intensity += (Time.deltaTime * BRIGHTNESS_CHANGE_PER_TICK);
    }


    private void DecrementLightBrightness()
    {
        GetLightGameObject().GetComponent<Light>().intensity -= (Time.deltaTime * BRIGHTNESS_CHANGE_PER_TICK);
    }


    private void HandleBrightnessChange()
    {
        if(get_brighter)
        {
            IncrementLightBrightness();
        }
        else if(get_dimmer)
        {
            DecrementLightBrightness();
        }
    }


    private void DisableLight()
    {
        GetLightGameObject().SetActive(false);
    }


    private void EnableLight()
    {
        GetLightGameObject().SetActive(true);
    }


    private bool IsDoneMovingToTarget()
    {
        Vector3 height_normalized_position = new Vector3(transform.position.x, 1f, transform.position.z);
        float dist = Vector3.Distance(move_target, height_normalized_position);

        if(recovery_grace_time <= 0f && dist == previous_target_distance)
        {
            state_to_recover_to = state;
            SetState(GSTATE.recover);
        }
        recovery_grace_time -= Time.deltaTime;
        previous_target_distance = dist;
        return dist < target_move_tollerance;
    }


    private void JumpToTarget() { JumpToTarget(false); }
    private void JumpToTarget(bool recovery_height)
    {
        float height = transform.position.y;
        if(recovery_height)
        {
            height = GameConfig.GHOST_SPAWN_HEIGHT;
        }

        Vector3 height_adjusted_target = new Vector3(move_target.x, height, move_target.z);
        transform.position = height_adjusted_target;
    }


    private void HandleStateNonMovement()
    {
        if(state == GSTATE.pause)
        {
            HandleBrightnessChange();
        }
    }


    private void HandleMovement()
    {
        if(state != GSTATE.waiting)
        {
            if(state == GSTATE.pause)
            {
                HardStop();
            }
            else if(state == GSTATE.ready)
            {
                SetState(GSTATE.scatter);
                DoNewTargetMovement();
            }
            else if(state == GSTATE.recover)
            {
                Debug.LogWarning(ghost_name +" entered recover state ("+ state_to_recover_to +")");
                HardStop();
                JumpToTarget(true);
                SetState(state_to_recover_to);
                DoNewTargetMovement();
            }
            else if(IsDoneMovingToTarget())
            {
                if(state == GSTATE.release)
                {
                    if(ai.IsFullyReleased())
                    {
                        Debug.Log("Released");
                        ReenablePlayerCollisions();
                        SetState(LevelManager.instance.GetTargetGhostState());
                    }
                }
                else if(state == GSTATE.eaten)
                {
                    if(ai.IsFullyEaten())
                    {
                        Debug.Log("Fully eaten");
                        ReenablePlayerCollisions();
                        SetState(GSTATE.release);
                        EnableLight();
                        ResetMaterial();
                    }
                }

                JumpToTarget();
                DoNewTargetMovement();
            }
            else
            {
                MoveToTarget();
            }
        }
    }


    private void FlipAround()
    {
        AssignNewMoveTarget(ai.GetFlipAroundMoveTarget());
        // Debug.Log("Flipping around");
        SetRotation();
    }


    private void DoNewTargetMovement()
    {
        AssignNewMoveTarget(ai.GetNextMoveTarget());
        // Debug.Log("Got new move target ("+ ai.GetLastMoveDirection() +"): "+ move_target);
        SetRotation();
    }


    private void AssignNewMoveTarget(Vector3 new_move_target)
    {
        move_target = new_move_target;
        previous_target_distance = 9999f;
        recovery_grace_time = RECOVERY_GRACE_TIME_RESET_SEC;
    }


    private void SetRotation()
    {
        float rotation_target = 0f;

        if(ai.GetLastMoveDirection() == "left")
        {
            rotation_target = -90f;
        }
        else if(ai.GetLastMoveDirection() == "right")
        {
            rotation_target = 90f;
        }
        else if(ai.GetLastMoveDirection() == "down")
        {
            rotation_target = 180f;
        }

        float current_y_adjusted = transform.eulerAngles.y;
        if(current_y_adjusted > 180f)
        {
            current_y_adjusted -= 360f;
        }

        if(current_y_adjusted != rotation_target)
        {
            // Debug.Log(current_y_adjusted +" != "+ rotation_target);
            HardStop();
            transform.eulerAngles = new Vector3(
                transform.eulerAngles.x,
                rotation_target,
                transform.eulerAngles.z
            );
        }
    }


    private void MoveToTarget()
    {
        float move_speed = GameConfig.GHOST_MOVE_SPEED;
        float top_speed = GameConfig.GHOST_TOP_SPEED;
        if(state == GSTATE.frightened)
        {
            top_speed -= GameConfig.FRIGHTENED_MOVE_SPEED_PENALTY;
        }
        else if(state == GSTATE.eaten)
        {
            move_speed *= 1.5f;
            top_speed *= 1.5f;
        }

        ForwardMovementAtSpeed(move_speed, top_speed);
    }


    protected override void WentThroughPortal()
    {
        DoNewTargetMovement();
    }


    public Vector2 GetLevelXY()
    {
        return Common.ThreeDToLevelCoords(transform.position);
    }


    private void ShowReleaseNotification()
    {
        NotificationHandler.instance.Notify(ghost_name +" released!", NameToNotificationStyle());
    }


    private void ShowEatenNotification()
    {
        NotificationHandler.instance.Notify(ghost_name +" eaten!", NameToNotificationStyle());
    }


    public NOTIFSTYLE NameToNotificationStyle()
    {
        if(ghost_name == "blinky")
        {
            return NOTIFSTYLE.blinky;
        }
        else if(ghost_name == "inky")
        {
            return NOTIFSTYLE.inky;
        }
        else if(ghost_name == "pinky")
        {
            return NOTIFSTYLE.pinky;
        }
        return NOTIFSTYLE.clyde;
    }
}
