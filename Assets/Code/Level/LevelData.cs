using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class LevelData
{
    public static int EMPTY = 0;
    public static int PELLET = 1;
    public static int POWER_PELLET = 2;
    public static int WALL = 4;
    public static int GATE = 5;
    public static int PORTAL = 6;
    public static int TALL_WALL = 7;
    
    public int difficulty;
    public int level_width;
    public int level_height;
    public int blinky_start_x;
    public int blinky_start_y;
    public int blinky_scatter_x;
    public int blinky_scatter_y;
    public int pinky_start_x;
    public int pinky_start_y;
    public int pinky_scatter_x;
    public int pinky_scatter_y;
    public int inky_start_x;
    public int inky_start_y;
    public int inky_scatter_x;
    public int inky_scatter_y;
    public int clyde_start_x;
    public int clyde_start_y;
    public int clyde_scatter_x;
    public int clyde_scatter_y;
    public int ghost_house_exit_x;
    public int ghost_house_exit_y;
    public int eaten_recovery_x;
    public int eaten_recovery_y;
    public int spawn_x;
    public int spawn_y;

    public int[] layout;


    public LevelData()
    {
        difficulty = -1;
        blinky_start_x = -1;
        blinky_start_y = -1;
        blinky_scatter_x = -1;
        blinky_scatter_y = -1;
        pinky_start_x = -1;
        pinky_start_y = -1;
        pinky_scatter_x = -1;
        pinky_scatter_y = -1;
        inky_start_x = -1;
        inky_start_y = -1;
        inky_scatter_x = -1;
        inky_scatter_y = -1;
        clyde_start_x = -1;
        clyde_start_y = -1;
        clyde_scatter_x = -1;
        clyde_scatter_y = -1;
        ghost_house_exit_x = -1;
        ghost_house_exit_y = -1;
        eaten_recovery_x = -1;
        eaten_recovery_y = -1;
        spawn_x = -1;
        spawn_y = -1;
    }


    public bool IsOutOfBounds(Vector2 pos) { return IsOutOfBounds((int)pos.x, (int)pos.y); }
    public bool IsOutOfBounds(int x, int y)
    {
        return (x >= level_width || y >= level_height || x < 0 || y < 0);
    }


    public static bool IsSolid(int const_val)
    {
        return (const_val == LevelData.WALL ||
                const_val == LevelData.TALL_WALL ||
                const_val == LevelData.GATE);
    }


    public bool IsSolid(int x, int y)
    {
        return IsSolid(GetData(x, y));
    }


    public bool IsSolidOrPortal(int x, int y)
    {
        int space_obj = GetData(x, y);

        return (space_obj == LevelData.WALL ||
                space_obj == LevelData.TALL_WALL ||
                space_obj == LevelData.GATE ||
                space_obj == LevelData.PORTAL);
    }


    public int XYToIndex(int x, int y)
    {
        if(layout.Length != (level_width * level_height))
        {
            Debug.LogError("MALFORMED LEVEL DATA ("+ layout.Length +" != "+ level_width +" * "+ level_height +")");
        }

        int x_inverse = level_width - x;
        int from_back = (y * level_width) + x_inverse;
        int index = layout.Length - from_back;

        return index;
    }


    public int GetData(int x, int y)
    {
        if(IsOutOfBounds(x, y))
        {
            return LevelData.EMPTY;
        }

        int index = XYToIndex(x, y);

        if(index >= layout.Length || index < 0)
        {
            return LevelData.EMPTY;
        }
        return layout[index];
    }


    public void SetData(int x, int y, int data)
    {
        // x = (level_width - 1) - x;  // Why???????
        if(IsOutOfBounds(x, y))
        {
            return;
        }

        int index = XYToIndex(x, y);

        if(index >= layout.Length || index < 0)
        {
            return;
        }

        layout[index] = data;
    }


    public Vector3 GetBottomLeftCoordOfLevelXY(int x, int y)
    {
        return GetBottomLeftCoordOfLevelXY(x, y, 1f);
    }

    public Vector3 GetBottomLeftCoordOfLevelXY(int x, int y, float height_offset)
    {
        return new Vector3((float)(x * GameConfig.SPACE_SIZE),
                           height_offset,
                           (float)(y * GameConfig.SPACE_SIZE));
    }


    public Vector3 GetBottomLeftCoordOfLevelXY(float x, float y)
    {
        return GetBottomLeftCoordOfLevelXY(x, y, 1f);
    }

    public Vector3 GetBottomLeftCoordOfLevelXY(float x, float y, float height_offset)
    {
        return new Vector3((x * ((float)GameConfig.SPACE_SIZE)),
                           height_offset,
                           (y * ((float)GameConfig.SPACE_SIZE)));
    }


    public Vector3 GetCenterCoordOfLevelXY(int x, int y)
    {
         return GetCenterCoordOfLevelXY(x, y, 1f);
    }


    public Vector3 GetCenterCoordOfLevelXY(int x, int y, float height_offset)
    {
        float half_space_size = ((float)GameConfig.SPACE_SIZE) / 2f;

        float logical_x_coord = ((float)(x * GameConfig.SPACE_SIZE)) + half_space_size;
        float logical_y_coord = ((float)(y * GameConfig.SPACE_SIZE)) + half_space_size;
        
        return new Vector3(logical_x_coord, height_offset, logical_y_coord);
    }


    protected string GetPrefabName(int level_data_constant)
    {
        string prefab_name = null;

        if(level_data_constant == LevelData.PELLET)
        {
            prefab_name = "Pellet";
        }
        else if(level_data_constant == LevelData.POWER_PELLET)
        {
            prefab_name = "PowerPellet";
        }
        else if(level_data_constant == LevelData.WALL)
        {
            prefab_name = "Wall";
        }
        else if(level_data_constant == LevelData.GATE)
        {
            prefab_name = "Gate";
        }
        else if(level_data_constant == LevelData.PORTAL)
        {
            prefab_name = "Portal";
        }
        else if(level_data_constant == LevelData.TALL_WALL)
        {
            prefab_name = "TallWall";
        }
        
        return prefab_name;
    }
}
