using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlayMenuFive : StartingMenu
{
    public static HowToPlayMenuFive instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        SetLocation(new Vector3(120f, 75f, -13f));
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void BackClicked()
    {
        TransitionToNewMenuSection(HowToPlayMenuFour.instance);
    }


    public void ReturnToMainMenuClicked()
    {
        BackToMain();
    }
}
