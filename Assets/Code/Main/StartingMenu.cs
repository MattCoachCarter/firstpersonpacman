using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingMenu : UIHelper
{
    private StartingMenu traveling_to = null;
    private Vector3 world_location;


    public Vector3 GetLocation()
    {
        return world_location;
    }


    protected void SetLocation(Vector3 loc)
    {
        world_location = loc;
    }


    public void Activate()
    {
        gameObject.SetActive(true);
    }


    public void Deactivate()
    {
        gameObject.SetActive(false);
    }


    public void ReachedOtherMenu()
    {
        traveling_to.Activate();
    }


    protected void TransitionToNewMenuSection(StartingMenu other_menu)
    {
        traveling_to = other_menu;
        MainCameraHandler.MoveToTarget(other_menu.GetLocation(), ReachedOtherMenu);
        Deactivate();
    }


    protected void BackToMain()
    {
        TransitionToNewMenuSection(MainMenu.instance);
    }
}
