using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingSplashScreen : MonoBehaviour
{
    public static LoadingSplashScreen instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Hide();
    }

    public static void Show()
    {
        instance.gameObject.SetActive(true);
    }

    public static void Hide()
    {
        instance.gameObject.SetActive(false);
    }
}
