using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlayMenuFour : StartingMenu
{
    public static HowToPlayMenuFour instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        SetLocation(new Vector3(100f, 82f, -13f));
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void BackClicked()
    {
        TransitionToNewMenuSection(HowToPlayMenuThree.instance);
    }


    public void NextClicked()
    {
        TransitionToNewMenuSection(HowToPlayMenuFive.instance);
    }
}
