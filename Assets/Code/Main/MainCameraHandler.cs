using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraHandler : MonoBehaviour
{
    public static MainCameraHandler instance;
    private GameObject camera_handle;
    private Vector3 target;
    private Vector3 start_pos;
    private string state = "idle";
    private Action callback;
    private const float APPROACH_SPEED = 200f;


    // Start is called before the first frame update
    void Start()
    {
        FindCamera();
        instance = this;
        SetStartingCameraPosition();
    }

    // Update is called once per frame
    void Update()
    {
        if(state == "moving")
        {
            DoMoveToTarget();
        }
    }


    private void SetStartingCameraPosition()
    {
        camera_handle.transform.position = new Vector3(0, 1, -13);
        camera_handle.transform.eulerAngles = new Vector3(0, 0, 0);
    }


    private void FindCamera()
    {
        camera_handle = GameObject.Find("Main Camera");
    }


    public static void MoveToTarget(Vector3 new_target) { MoveToTarget(new_target, null); }
    public static void MoveToTarget(Vector3 new_target, Action cb)
    {
        instance.callback = cb;
        instance.start_pos = instance.camera_handle.transform.position;
        instance.target = new_target;
        instance.state = "moving";
    }


    private void DoMoveToTarget()
    {
        float original_distance = Vector3.Distance(start_pos, target);
        float distance_covered = Vector3.Distance(start_pos, camera_handle.transform.position);
        float pct_done_ratio = distance_covered / original_distance;
        float easing_modifier = (Mathf.Sin(Mathf.PI * pct_done_ratio) + 0.1f) * 1.76f;
        easing_modifier *= easing_modifier;

        float approach_amount = (APPROACH_SPEED * easing_modifier) * Time.deltaTime;
        float new_x = Common.ApproachTarget(camera_handle.transform.position.x, approach_amount, target.x);
        float new_y = Common.ApproachTarget(camera_handle.transform.position.y, approach_amount, target.y);
        float new_z = Common.ApproachTarget(camera_handle.transform.position.z, approach_amount, target.z);

        Vector3 new_pos = new Vector3(new_x, new_y, new_z);
        camera_handle.transform.position = new_pos;

        if(new_pos == target)
        {
            state = "idle";
            if(callback != null)
            {
                callback();
            }
        }
    }
}
