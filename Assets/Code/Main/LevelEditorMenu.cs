using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditorMenu : LevelSelectMenu
{
    public new static LevelEditorMenu instance;

    protected override void Awake() {}


    // Start is called before the first frame update
    protected override void Start()
    {
        instance = this;
        mode = "custom";
        SetLocation(new Vector3(50f, 10f, -13f));
        ShowLevels();
        Deactivate();
    }


    protected override List<string> GetCurrentLeveList()
    {
        return GameManager.GetCustomLevelList();
    }


    public void NewClicked()
    {
        GameManager.EditLevel(null);
    }


    public override void SelectLevel(int level_button_index)
    {
        LoadingSplashScreen.Show();
        List<string> level_list = GetCurrentLeveList();
        int level_index = scroll_offset + level_button_index;
        GameManager.EditLevel(level_list[level_index]);
    }
}
