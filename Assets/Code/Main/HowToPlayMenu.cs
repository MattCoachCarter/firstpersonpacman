using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlayMenu : StartingMenu
{
    public static HowToPlayMenu instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        SetLocation(new Vector3(30f, 30f, -13f));
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void BackClicked()
    {
        BackToMain();
    }


    public void NextClicked()
    {
        TransitionToNewMenuSection(HowToPlayMenuTwo.instance);
    }
}
