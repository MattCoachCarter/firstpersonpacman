using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBestDisplay : UIHelper
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Setup(string level_name, string mode)
    {
        UserData user_data = GameManager.GetUserData();

        LevelBest best = null;
        if(mode == "custom")
        {
            if(user_data.custom_level_bests != null)
            {
                foreach(LevelBest lb in user_data.custom_level_bests)
                {
                    if(lb.GetName() == level_name)
                    {
                        best = lb;
                        break;
                    }
                }
            }
        }
        else
        {
            if(user_data.default_level_bests != null)
            {
                foreach(LevelBest lb in user_data.default_level_bests)
                {
                    if(lb.GetName() == level_name)
                    {
                        best = lb;
                        break;
                    }
                }
            }
        }

        if(best == null)
        {
            Deactivate();
        }
        else
        {
            Activate();
            ShowBest(best);
        }
    }


    private void ShowBest(LevelBest best)
    {
        SetChildText("BestTime", best.Format());
    }


    public void Deactivate()
    {
        gameObject.SetActive(false);
    }


    protected void Activate()
    {
        gameObject.SetActive(true);
    }
}
