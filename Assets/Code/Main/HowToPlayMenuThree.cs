using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlayMenuThree : StartingMenu
{
    public static HowToPlayMenuThree instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        SetLocation(new Vector3(70f, 70f, -13f));
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void BackClicked()
    {
        TransitionToNewMenuSection(HowToPlayMenuTwo.instance);
    }


    public void NextClicked()
    {
        TransitionToNewMenuSection(HowToPlayMenuFour.instance);
    }
}
