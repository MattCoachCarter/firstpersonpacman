using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlayMenuTwo : StartingMenu
{
    public static HowToPlayMenuTwo instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        SetLocation(new Vector3(30f, 75f, -13f));
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void BackClicked()
    {
        TransitionToNewMenuSection(HowToPlayMenu.instance);
    }


    public void NextClicked()
    {
        TransitionToNewMenuSection(HowToPlayMenuThree.instance);
    }
}
