using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenu : StartingMenu
{
    public static OptionsMenu instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        SetLocation(new Vector3(25f, -40f, -13f));
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void BackClicked()
    {
        BackToMain();
    }
}
