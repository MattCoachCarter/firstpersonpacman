using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectMenu : StartingMenu
{
    public static LevelSelectMenu instance;
    protected string mode = "default";
    protected string other_mode = "custom";
    protected int scroll_offset = 0;
    protected float scroll_cooldown = 0f;
    protected const int LEVEL_SHOW_COUNT = 5;
    protected const float SCROLL_COOLDOWN_RESET = 0.1f;
    private List<LevelBestDisplay> level_best_displays = null;


    protected virtual void Awake()
    {
        GetLevelBestDisplays();
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        instance = this;
        SetLocation(new Vector3(-50f, 1f, -13f));
        ShowLevels();
        Deactivate();
    }


    protected virtual void Update()
    {
        HandleScrolling();
    }


    private void GetLevelBestDisplays()
    {
        level_best_displays = new List<LevelBestDisplay>();
        level_best_displays.Add(GetChild("Level_0_Best").GetComponent<LevelBestDisplay>());
        level_best_displays.Add(GetChild("Level_1_Best").GetComponent<LevelBestDisplay>());
        level_best_displays.Add(GetChild("Level_2_Best").GetComponent<LevelBestDisplay>());
        level_best_displays.Add(GetChild("Level_3_Best").GetComponent<LevelBestDisplay>());
        level_best_displays.Add(GetChild("Level_4_Best").GetComponent<LevelBestDisplay>());
    }


    private void HideAllLevelBestDisplays()
    {
        if(level_best_displays != null)
        {
            foreach(LevelBestDisplay lbd in level_best_displays)
            {
                lbd.Deactivate();
            }
        }
    }


    public void BackClicked()
    {
        BackToMain();
    }


    public void SwitchMode()
    {
        string temp = other_mode;
        other_mode = mode;
        mode = temp;
        SetButtonText("ModeSwitchBtn", "see "+ other_mode +" levels");
        ShowLevels();
    }


    protected virtual List<string> GetCurrentLeveList()
    {
        List<string> level_list = GameManager.GetDefaultLevelList();
        if(mode == "custom")
        {
            level_list = GameManager.GetCustomLevelList();
        }

        return level_list;
    }


    protected void ShowLevels()
    {
        List<string> level_list = GetCurrentLeveList();
        HideAllLevelBestDisplays();

        if(level_list.Count > 0)
        {
            HideNoLevelsFoundText();
        }
        else
        {
            ShowNoLevelsFoundText();
        }
        
        for(int i = 0; i < LEVEL_SHOW_COUNT; i++)
        {
            int level_index = i + scroll_offset;
            string button_name = "Level_"+ i;

            if(level_index >= level_list.Count)
            {
                DeactivateChild(button_name);
            }
            else
            {
                ActivateChild(button_name);
                string raw_lvl_name = Common.GetLevelNameFromFilePath(level_list[level_index]);
                string formatted_level_name = raw_lvl_name;
                if(mode == "default")
                {
                    formatted_level_name = "level "+ formatted_level_name;
                }

                if(level_best_displays != null)
                {
                    level_best_displays[i].Setup(raw_lvl_name, mode);
                }
                SetButtonText(button_name, formatted_level_name);
            }
        }
    }


    public virtual void SelectLevel(int level_button_index)
    {
        LoadingSplashScreen.Show();
        List<string> level_list = GetCurrentLeveList();
        int level_index = scroll_offset + level_button_index;
        GameManager.SetInCustomLevel(mode == "custom");
        GameManager.LoadLevel(level_list[level_index]);
    }


    protected bool CanScroll()
    {
        return scroll_cooldown <= 0f;
    }


    public void ScrollDown()
    {
        if(!CanScroll()) { return; }
        scroll_cooldown = SCROLL_COOLDOWN_RESET;

        List<string> level_list = GetCurrentLeveList();
        if((scroll_offset + LEVEL_SHOW_COUNT) < level_list.Count)
        {
            scroll_offset++;
        }
        ShowLevels();
    }


    public void ScrollUp()
    {
        if(!CanScroll()) { return; }
        scroll_cooldown = SCROLL_COOLDOWN_RESET;

        if(scroll_offset > 0)
        {
            scroll_offset--;
        }
        ShowLevels();
    }


    protected void HandleScrolling()
    {
        scroll_cooldown = Common.ApproachZero(scroll_cooldown, Time.deltaTime);

        if(Input.mouseScrollDelta.y > 0)
        {
            ScrollUp();
        }
        else if(Input.mouseScrollDelta.y < 0)
        {
            ScrollDown();
        }
    }


    protected void ShowNoLevelsFoundText()
    {
        ActivateChild("NoLevelsFoundText");
    }


    protected void HideNoLevelsFoundText()
    {
        DeactivateChild("NoLevelsFoundText");
    }
}
