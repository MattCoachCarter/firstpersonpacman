using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : StartingMenu
{
    public static MainMenu instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        SetLocation(new Vector3(0f, 1f, -13f));
    }


    void Update()
    {
        if(GameManager.jump_to_level_select)
        {
            GameManager.jump_to_level_select = false;
            TransitionToNewMenuSection(LevelSelectMenu.instance);
        }
    }


    public void LevelSelectClicked()
    {
        TransitionToNewMenuSection(LevelSelectMenu.instance);
    }


    public void HowToPlayClicked()
    {
        TransitionToNewMenuSection(HowToPlayMenu.instance);
    }


    public void LevelEditorClicked()
    {
        TransitionToNewMenuSection(LevelEditorMenu.instance);
    }


    public void OptionsClicked()
    {
        TransitionToNewMenuSection(OptionsMenu.instance);
    }


    public void ExitClicked()
    {
        Application.Quit();
    }
}
