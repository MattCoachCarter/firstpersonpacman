using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Reflection;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    private const int MAIN_SCENE = 0;
    private const int LEVEL_SCENE = 1;
    private const int EDITOR_SCENE = 2;

    public static GameManager instance = null;  // This is for handling singletons properly
    private string pending_level = null;
    private List<string> default_levels;
    private List<string> custom_levels;
    private bool testing_custom_level = false;
    private LevelData testing_level_data = null;
    private static UserData user_data = null;
    private static bool in_custom_level = false;
    public static bool jump_to_level_select = false;


    /**
     * This is called when the game starts for the first time. Called once and only once
     */
    void Awake()
    {
        Debug.Log("GameManager: Awake");
        if(HandleSingleton())
        {
            CreateInputManager();
            GetAllLevelFiles();
            user_data = JSONParser.GetUserData();
        }
    }


    void Start()
    {
        Debug.Log("GameManager: Start");
    }


    public static void SetInCustomLevel(bool val)
    {
        in_custom_level = val;
    }


    public static bool GetInCustomLevel()
    {
        return in_custom_level;
    }


    public static void WriteUserData()
    {
        JSONParser.WriteUserData(user_data);
    }


    public static UserData GetUserData()
    {
        return user_data;
    }


    public static void SetLevelTestMode()
    {
        instance.testing_custom_level = true;
    }


    public static void UnsetLevelTestMode()
    {
        instance.testing_custom_level = false;
    }


    public static bool IsInLevelTestMode()
    {
        return instance.testing_custom_level;
    }


    public static void RefreshLevelLists()
    {
        instance.GetAllLevelFiles();
    }


    private void GetAllLevelFiles()
    {
        default_levels = new List<string>();
        custom_levels = new List<string>();

        string default_level_dir = Path.Combine(Application.streamingAssetsPath, GameConfig.DEFAULT_LEVEL_DIR);
        foreach(string file_path in Directory.GetFiles(default_level_dir))
        {
            if(file_path.EndsWith(".json"))
            {
                default_levels.Add(file_path);
            }
        }

        JSONParser.EnsureCustomLevelDirExists();
        foreach(string file_path in Directory.GetFiles(JSONParser.GetCustomLevelDir()))
        {
            if(file_path.EndsWith(".json"))
            {
                custom_levels.Add(file_path);
            }
        }

        default_levels.Sort(delegate(string a, string b)
            {
                string a_name = Path.GetFileName(a).Replace(".json", "");
                string b_name = Path.GetFileName(b).Replace(".json", "");
                if(int.Parse(a_name) > int.Parse(b_name))
                {
                    return 1;
                }
                return -1;
            }
        );

        custom_levels.Sort();

        Debug.Log("Found "+ default_levels.Count +" default levels");
        Debug.Log("Found "+ custom_levels.Count +" custom levels");
    }


    public static List<string> GetDefaultLevelList()
    {
        return instance.default_levels;
    }


    public static List<string> GetCustomLevelList()
    {
        return instance.custom_levels;
    }


    /**
     * Make sure we treat this class/object as a singleton, so there can be only one
     */
    bool HandleSingleton()
    {
        if(instance == null)
        {
            instance = this;
            Debug.Log("Setting GameManager to DontDestroyOnLoad");
            DontDestroyOnLoad(gameObject);
        }
        else if(instance != this)
        {
            Destroy(gameObject); // Don't spawn, there's already a game manager!
            return false;
        }

        return true;
    }


    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        Debug.Log(mode);
    }


    private void CreateInputManager()
    {
        Common.Spawn("InputManager");
    }


    public static string GetPendingLevel()
    {
        return instance.pending_level;
    }


    private static void LoadLevelScene()
    {
        Debug.Log("Loading Level Scene");
        SceneManager.LoadScene(LEVEL_SCENE);
    }


    private static void LoadEditorScene()
    {
        Debug.Log("Loading Editor Scene");
        SceneManager.LoadScene(EDITOR_SCENE);
    }


    private static void LoadMainScene()
    {
        Debug.Log("Loading Main Scene");
        SceneManager.LoadScene(MAIN_SCENE);
    }


    public static void LoadLevel(string level_file)
    {
        instance.pending_level = level_file;
        LoadLevelScene();
    }


    public static void EditLevel(string level_file)
    {
        instance.pending_level = level_file;
        LoadEditorScene();
    }


    public static void ReturnToMainMenu()
    {
        // UnsetLevelTestMode();
        LoadMainScene();
        RefreshLevelLists();
    }


    public static void ReturnFromTestMode()
    {
        SetLevelTestMode();
        LoadEditorScene();
    }


    public static void TestLevel(LevelData level_data)
    {
        SetLevelTestMode();
        instance.testing_level_data = level_data;
        LoadLevelScene();
    }


    public static LevelData GetLevelTestData()
    {
        return instance.testing_level_data;
    }
}
