using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfig
{
    // START HANDLING FOR SINGLETON
    private static GameConfig instance = new GameConfig();  // Calls constructor
    public static GameConfig Instance // Use to access the singleton
    {
        get { return instance; }
    }
    // END HANDLING FOR SINGLETON


    private GameConfig()  // Constructor, called in singleton logic
    {
        // Constructor
        // TODO: Load user config
    }


    ///////////////////////////////////////////////////////////////////////
    // Constant, not user settable:
    ///////////////////////////////////////////////////////////////////////
    public static bool DEBUG = true;


    // GAMEPLAY/BALANCING
    public static int SPACE_SIZE = 10;
    public static float PLAYER_SPAWN_HEIGHT = 2.1f;
    public static float PLAYER_MOVE_SPEED = 60f;
    public static float PLAYER_TOP_SPEED = 25f;
    public static float GHOST_MOVE_SPEED = 50f;
    public static float GHOST_TOP_SPEED = 22f;
    public static float FRIGHTENED_MOVE_SPEED_PENALTY = 6f;
    public static float ROTATION_SPEED = 10f;
    public static float BASE_FRIGHTEN_TIME = 11f;
    public static Vector3 CAMERA_PLAYER_OFFSET = new Vector3(0, 2f, 0.1f);
    public static float GHOST_SPAWN_HEIGHT = 3.5f;
    public static float BASE_PELLET_RELEASE_A = 0.13f;
    public static float BASE_PELLET_RELEASE_B = 0.37f;
    public static int MINIMAP_WIDTH = 11;
    public static int MINIMAP_HEIGHT = 9;
    public static float PELLET_MINIMAP_TIME = 0.5f;
    public static float POWER_PELLET_MINIMAP_TIME = 5f;
    public static float NOTIFICATION_TIME = 1.5f;
    public static float LEVEL_LOSE_DELAY = 5f;
    public static float GHOST_RELEASE_COOLDOWN = 8f;
    public static string DEFAULT_LEVEL_DIR = "Levels/Default";
    public static string CUSTOM_LEVEL_DIR = "CustomLevels";
    public static string TEST_LEVEL_FILE = "editor_tmp_save.json";
    public static string TEST_LEVEL_FILE_NAME_FILE = "editor_tmp_save.txt";
    public static string USER_DATA_FILE = "user_data.json";
    public static int DEFAULT_LEVEL_WIDTH = 41;
    public static int DEFAULT_LEVEL_HEIGHT = 41;
    public static float BASE_CHASE_TIME = 26f;
    public static float BASE_SCATTER_TIME = 12f;
    public static float LEVEL_CAMERA_SWEEP_Y_START = 200f;
    public static float LEVEL_START_DELAY = 1f;
    public static float SOLID_COLLIDER_OVERSIZE_TARGET = 2.5f;
    public static float EXPLODE_DELAY = 0.5f;


    // CONTROLS:
    public static int BTN_DOUBLE_TAP_MIN_FRAME_GAP = 2;
    public static int BTN_DOUBLE_TAP_MAX_FRAME_GAP = 10;
    public static int MAX_INPUT_HISTORY = 51;
    public static Dictionary<KeyCode, BTNINPUT> CONTROL_CONFIG = new Dictionary<KeyCode, BTNINPUT>()
    {
        { KeyCode.W, BTNINPUT.up },
        { KeyCode.S, BTNINPUT.down },
        { KeyCode.A, BTNINPUT.left },
        { KeyCode.D, BTNINPUT.right },
        { KeyCode.LeftShift, BTNINPUT.stop },
        { KeyCode.Space, BTNINPUT.camera_flip },
        { KeyCode.P, BTNINPUT.toggle_debug },
        { KeyCode.L, BTNINPUT.debug_pellet },
        { KeyCode.E, BTNINPUT.zoom_in },
        { KeyCode.Q, BTNINPUT.zoom_out },
        { KeyCode.Escape, BTNINPUT.pause },
        { KeyCode.Alpha0, BTNINPUT.hotkey0 },
        { KeyCode.Alpha1, BTNINPUT.hotkey1 },
        { KeyCode.Alpha2, BTNINPUT.hotkey2 },
        { KeyCode.Alpha3, BTNINPUT.hotkey3 },
        { KeyCode.Alpha4, BTNINPUT.hotkey4 },
        { KeyCode.Alpha5, BTNINPUT.hotkey5 },
        { KeyCode.Alpha6, BTNINPUT.hotkey6 },
        { KeyCode.Alpha7, BTNINPUT.hotkey7 },
        { KeyCode.Alpha8, BTNINPUT.hotkey8 },
        { KeyCode.Alpha9, BTNINPUT.hotkey9 }
    };


    ///////////////////////////////////////////////////////////////////////
    // User settable:
    ///////////////////////////////////////////////////////////////////////
    public int x_res = 1280;
    public int y_res = 720;
    public int vsync = 1;
    public float mouse_x_sensitivity = 2.5f;
    public float mouse_y_sensitivity = 2.5f;
    public float mouse_invert_y = -1f;  // Negative is NOT inverted
}
