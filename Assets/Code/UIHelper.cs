using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;


public class UIHelper : MonoBehaviour
{
    protected UIHelper AttachToCanvas()
    {
        transform.SetParent(GetCanvas().transform, false);
        return this;
    }


    protected GameObject GetCanvas()
    {
        return GameObject.Find("Canvas");
    }


    public GameObject GetChild(string child_name)
    {
        Transform t = transform.Find(child_name);
        if(t != null)
        {
            return t.gameObject;
        }

        Debug.LogError("Could not find child with name: "+ child_name);
        return null;
    }


    public void SetChildText(string child_name, string t)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            child_obj.GetComponent<Text>().text = t;
        }
    }


    public void SetButtonText(string child_name, string t)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            child_obj.GetComponent<UIHelper>().SetChildText("Text", t);
        }
    }


    public void SelectButton(string child_name)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            child_obj.GetComponent<Button>().Select();
        }
    }


    public void SetChildTextColor(string child_name, Color c)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            child_obj.GetComponent<Text>().color = c;
        }
    }


    public void FlipChild(string child_name)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            Vector3 scale_change = new Vector3(2, 0, 0);
            if(child_obj.transform.localScale.x > 0)
            {
                scale_change.x = -2;
            }

            child_obj.transform.localScale += scale_change;
        }
        else
        {
            Debug.LogError("Tried to FlipChild but child with name \""+ child_name +"\" does not exist");
        }
    }


    public void InvertChildX(string child_name)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            Vector3 target_scale = new Vector3(-1, 1, 1);
            child_obj.transform.localScale = target_scale;
        }
        else
        {
            Debug.LogError("Tried to InvertChildX but child with name \""+ child_name +"\" does not exist");
        }
    }


    public void UninvertChildX(string child_name)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            Vector3 target_scale = new Vector3(1, 1, 1);
            child_obj.transform.localScale = target_scale;
        }
        else
        {
            Debug.LogError("Tried to UninvertChildX but child with name \""+ child_name +"\" does not exist");
        }
    }


    public void ActivateChild(string child_name)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            child_obj.SetActive(true);
        }
        else
        {
            Debug.LogError("Tried to ActivateChild but child with name \""+ child_name +"\" does not exist");
        }
    }


    public void DeactivateChild(string child_name)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            child_obj.SetActive(false);
        }
        else
        {
            Debug.LogError("Tried to ActivateChild but child with name \""+ child_name +"\" does not exist");
        }
    }


    private void SetObjImage(GameObject obj, Sprite spr)
    {
        obj.GetComponent<Image>().sprite = spr;
    }


    private void SetObjImageAlpha(GameObject obj, float new_alpha)
    {
        Color temp = obj.GetComponent<Image>().color;
        temp.a = new_alpha;
        obj.GetComponent<Image>().color = temp;
    }


    public void SetChildImage(string child_name, string i, float img_alpha)
    {
        Sprite sprite = Common.LoadSprite(i);
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            SetObjImage(child_obj, sprite);
            SetObjImageAlpha(child_obj, img_alpha);
        }
        else
        {
            Debug.LogError("Tried to SetChildImage but child with name \""+ child_name +"\" does not exist");
        }
    }


    public void SetChildImage(string child_name, string i)
    {
        Sprite sprite = Common.LoadSprite(i);
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            SetObjImage(child_obj, sprite);
        }
        else
        {
            Debug.LogError("Tried to SetChildImage but child with name \""+ child_name +"\" does not exist");
        }
    }


    public void SetChildRawImage(string child_name, Texture2D t)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            child_obj.GetComponent<RawImage>().texture = t;
        }
        else
        {
            Debug.LogError("Tried to SetChildRawImage but child with name \""+ child_name +"\" does not exist");
        }
    }


    public void ChangeChildImageAlpha(string child_name, float new_alpha)
    {
        GameObject child_obj = GetChild(child_name);
        if(child_obj != null)
        {
            SetObjImageAlpha(child_obj, new_alpha);
        }
        else
        {
            Debug.LogError("Tried to SetChildImage but child with name \""+ child_name +"\" does not exist");
        }
    }


    public void DestroySelf()
    {
        Common.SafeDestroy(gameObject);
    }


    public Vector3 GetPosition()
    {
        return gameObject.GetComponent<RectTransform>().localPosition;
    }


    public Vector3 GetChildPosition(string child_name)
    {
        return GetChild(child_name).GetComponent<RectTransform>().localPosition;
    }


    public UIHelper SetPosition(Vector3 screen_position)
    {
        gameObject.GetComponent<RectTransform>().localPosition = screen_position;
        return this;
    }


    public void SetChildPosition(string child_name, Vector3 screen_position)
    {
        GetChild(child_name).GetComponent<RectTransform>().localPosition = screen_position;
    }
}
