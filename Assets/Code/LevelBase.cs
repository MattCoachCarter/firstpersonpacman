using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBase : MonoBehaviour
{
    protected LevelData level_data = null;


    protected LevelData ParseLevelData(string lfile)
    {
        return JSONParser.ParseLevelData(lfile);
    }


    protected GameObject SpawnFloor() { return SpawnFloor(0f); }
    protected GameObject SpawnFloor(float position_height)
    {
        float floor_height = ((float)(level_data.level_height  + 1)) * (GameConfig.SPACE_SIZE);
        float floor_width = ((float)(level_data.level_width  + 1)) * (GameConfig.SPACE_SIZE);
        float floor_spawn_x = (((float)(level_data.level_width - 1)) * (GameConfig.SPACE_SIZE)) / 2f;
        float floor_spawn_y = (((float)(level_data.level_height - 1)) * (GameConfig.SPACE_SIZE)) / 2f;

        Vector3 floor_scale = new Vector3(floor_width, 1f, floor_height);
        Vector3 spawn_position = new Vector3(floor_spawn_x, position_height, floor_spawn_y);

        GameObject floor_game_obj = Common.Spawn("Floor", spawn_position);
        floor_game_obj.transform.localScale = floor_scale;
        return floor_game_obj;
    }


    protected float GetSpawnY(int level_data_constant)
    {
        float y_val = 1f;

        if(level_data_constant == LevelData.PELLET)
        {
            y_val = 3f;
        }
        else if(level_data_constant == LevelData.POWER_PELLET)
        {
            y_val = 3.5f;
        }
        else if(level_data_constant == LevelData.WALL)
        {
            y_val = 6f;
        }
        else if(level_data_constant == LevelData.GATE)
        {
            y_val = 6f;
        }
        else if(level_data_constant == LevelData.PORTAL)
        {
            y_val = 6f;
        }
        else if(level_data_constant == LevelData.TALL_WALL)
        {
            y_val = 16f;
        }
        
        return y_val;
    }


    protected string GetPrefabName(int level_data_constant)
    {
        string prefab_name = null;

        if(level_data_constant == LevelData.PELLET)
        {
            prefab_name = "Pellet";
        }
        else if(level_data_constant == LevelData.POWER_PELLET)
        {
            prefab_name = "PowerPellet";
        }
        else if(level_data_constant == LevelData.WALL)
        {
            prefab_name = "Wall";
        }
        else if(level_data_constant == LevelData.GATE)
        {
            prefab_name = "Gate";
        }
        else if(level_data_constant == LevelData.PORTAL)
        {
            prefab_name = "Portal";
        }
        else if(level_data_constant == LevelData.TALL_WALL)
        {
            prefab_name = "TallWall";
        }
        
        return prefab_name;
    }
}
